% Chapter Template

\chapter{Small strains} % Main chapter title

\label{AppendixA} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}
%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Elements of classical (small strains) plasticity}
\subsection{Plasticity in small strains}
For the sake of simplicity, we will restrict ourselves to small strains, that is we consider that the reference configuration and the current configuration are undistinguishable. In particular, this implies that $\mathbf{P} \approx \Cstress$, and $J \approx 1$.\\
 In small strains plasticity it is common practice( for more details see also \cite{DeSouzaMathematical}, \cite{DeSouzaNumerics}, \cite{Simo1998Theory}, \cite{Simo1998Numerics1} ) to split the strain tensor $\strain$ in two i.e:
\begin{equation}
\strain = \estrain + \pstrain \, ,
\end{equation}  
where $\estrain$ is referred to as the elastic strain tensor and $\pstrain$ is the plastic strain tensor. We also split the energy functional in two, one part is contributing to the internal energy and the other part is contributing to the plastic dissipation. Therefore, we can write:
\begin{equation}
\psi(\strain, \pstrain, \boldsymbol{\al}) = \psi^{e}(\strain - \pstrain) + \psi^p(\boldsymbol{\al}) =\psi^{e}(\estrain) + \psi^p(\boldsymbol{\al}) \, .
\end{equation}
We also assume purely mechanical process (no thermal effect). Then, under those assumptions, the Clausius-Duhem inequality \eqref{Clausius-Duhem} becomes:
\begin{equation}
\paren{\Cstress - \rho\pder{\psi^{e}}{\estrain}} : \dot{\boldsymbol{\varepsilon}}^{e} + \Cstress : \dot{\boldsymbol{\varepsilon}}^{p} - \mathbf{A}*\dot{\boldsymbol{\al}} \geq 0 \, .
\label{CD_small}
\end{equation} 
We then define, accordingly with the general theory:
\[
\Cstress = \rho\pder{\psi^{e}}{\estrain} \, ,
\]
and, 
\[
\mathbf{A} = \rho\pder{\psi^{p}}{{\boldsymbol{\al}}} \, .
\]
Next we define the plastic dissipation functional:
\begin{equation}
\Upsilon^{p}(\Cstress, \mathbf{A}; \dot{\boldsymbol{\varepsilon}}^{p}, \dot{\boldsymbol{\al}} ) = \Cstress : \dot{\boldsymbol{\varepsilon}}^{p} - \mathbf{A}*\dot{\boldsymbol{\al}} \geq 0 \, .
\end{equation}
We then define the so called yield function, that is, the functional:
\begin{equation}
\phi \equiv \phi(\Cstress, \, \mathbf{A}) \, ,
\end{equation}
that is defining the set of all admissible states i.e we constrain the model such that it accepts only as admissible stresses the elements of the following domain:
\begin{equation}
\mathcal{E}_{adm} = \setbra{\left. \Cstress\in \mathbb{S} \right|  \phi(\Cstress, \mathbf{A}) \leq 0} \, ,
\label{adm_domain}
\end{equation}
where $\mathbb{S}$ is the space of symmetric second order tensors i.e:
\begin{equation}
\mathbb{S} = \setbra{\left. \mathbf{B} :  \R^{n} \times \R^{n} \rightarrow \R \right| \mathbf{B} \ \mathrm{is \ linear \ and} \ \mathbf{B}^{\mathrm{T}}=\mathbf{B} } \, .
\label{symm_tens}
\end{equation}
The interior of the admissible domain \eqref{adm_domain} is called the elastic domain i.e:
\begin{equation}
\mathrm{int}\paren{\mathcal{E}_{adm}} = \setbra{\left. \Cstress\in \mathbb{S} \right|  \phi(\Cstress, \mathbf{A}) < 0} \, ,
\end{equation}
and the hypersurface defined by:
\begin{equation}
\partial\mathcal{E}_{adm} = \setbra{\left. \Cstress\in \mathbb{S} \right|  \phi(\Cstress, \mathbf{A}) = 0} \, ,
\end{equation}
is called the yield surface.\\
According to the principle of maximum energy dissipation, the actual state of the system is the one maximising the plastic dissipation functional in the set of admissible states. That is, we have the constrained model:
 \begin{equation}
\begin{array}{r c l}
\begin{aligned}
\mathrm{maximise \ }& \Upsilon^{p}(\Cstress, \mathbf{A}; \dot{\boldsymbol{\varepsilon}}^{p}, \dot{\boldsymbol{\al}} ) \\
\mathrm{subject \ to \ }& \phi(\Cstress, \mathbf{A}) \leq 0
\end{aligned}
\end{array}
\label{const_pb}
\end{equation}
If we set the Lagrangian the following way, 
\begin{equation}
\mathcal{L}(\Cstress, \mathbf{A} \, ; \, \dot{\boldsymbol{\varepsilon}}^{p}, \dot{\boldsymbol{\al}}  ) = \Upsilon^{p}(\Cstress, \mathbf{A}; \dot{\boldsymbol{\varepsilon}}^{p}, \dot{\boldsymbol{\al}} ) - \dot{\ga}\phi(\Cstress, \mathbf{A}) \, ,
\end{equation}
where the Lagrange multiplier $\dot{\ga}$ is called the plastic multiplier, we can now write the Euler-Lagrange equations:
\begin{equation}
\pder{\mathcal{L}}{\Cstress} = 0 \Leftrightarrow \dot{\boldsymbol{\varepsilon}}^{p} - \dot{\ga}\pder{\phi}{\Cstress} = 0 \Leftrightarrow \dot{\boldsymbol{\varepsilon}}^{p} = \dot{\ga}\pder{\phi}{\Cstress} \, ,
\label{evo_equa1}
\end{equation}
and 
\begin{equation}
\pder{\mathcal{L}}{\mathbf{A}} = 0 \Leftrightarrow \dot{\boldsymbol{\al}} - \dot{\ga}\pder{\phi}{\mathbf{A}} = 0 \Leftrightarrow \dot{\boldsymbol{\al}} = -\dot{\ga}\pder{\phi}{\mathbf{A}} \, .
\label{evo_equa2}
\end{equation}
The tensor $\mathbf{N} := \pder{\phi}{\Cstress} $ is called the (associative) flow vector and $\mathbf{H} := -\pder{\phi}{\mathbf{A}}$ is called the (associative) generalised hardening modulus. Although the evolution equations \eqref{evo_equa1} and \eqref{evo_equa2} are derived from the principle of maximum plastic dissipation, it is possible to choose a flow potential that is different from the yield function $\phi$ to match the experimental behaviour of some materials for some particular cases.

In addition to the evolution equations \eqref{evo_equa1} and \eqref{evo_equa2} we had the so-called Kuhn-Tucker conditions:
\begin{equation}
\phi \leq 0, \ \dot{\ga} \geq 0, \ \phi\dot{\ga} = 0 \, ,
\label{KKT}
\end{equation}
which gave us the loading/unloading conditions. From a physical point of view, the equations \eqref{KKT} imply that material can yield only its stress state is on the yield surface i.e $\phi(\Cstress, \mathbf{A}) = 0$.
\subsubsection{The Von-Mises model with isotropic linear hardening}
The Von-Mises model is an isotropic model based on the following explicit definition of the yield function:
\begin{equation}
\phi(\Cstress, \mathbf{A}) = \sqrt{3J_2(\mathbf{s})} - \sigma_y \, ,
\end{equation}
where $\mathbf{s}$ is the deviatoric stress defined by:
\begin{equation}
\mathbf{s} := \paren{\mathbb{I}- \frac{1}{3}\mathbf{I} \otimes \mathbf{I}} : \Cstress = \Cstress - \frac{1}{3}\mathrm{tr}(\Cstress)\mathbf{I} \, ,
\end{equation}
and $J_2(\mathbf{s})$ is the invariant defined by:
\begin{equation}
J_2(\mathbf{s}) = -I_2(\mathbf{s}) = \mathrm{tr}(\mathbf{s}^2) - \mathrm{tr}^2(\mathbf{s}) = \mathrm{tr}(\mathbf{s}^2) \, .
\end{equation}
computing the flow rule using the Von-Mises yield function, one may obtain:
\begin{equation}
\mathbf{N } = \pder{\phi}{\Cstress} = \sqrt{\frac{3}{2}}\frac{\mathbf{s}}{\norm{\mathbf{s}}}
\end{equation}
And therefore :
\begin{equation}
\dot{\boldsymbol{\varepsilon}}^p= \dot{\ga}\sqrt{\frac{3}{2}}\frac{\mathbf{s}}{\norm{\mathbf{s}}}
\end{equation}
\subsection{The return-mapping algorithm in small strains}
Several improvements have been provided to the code written during the first year. First, an algorithm using mixed hardening has been implemented. Now the plastic module in MoFEM can deal with isotropic and kinematic linear hardening. Also, a time solver has been implemented and adaptive time stepping can be triggered using Petsc features (for more details, see also [...]). The time solver was also used to implement a viscoplastic model. A simplified version of the return-mapping algorithm is presented in figure \ref{MoFEM_implementation_2}.\\
Also several issues due to pure technical implementation have been solved. In particular, it is now possible to use polynomial of order higher than 1, thus using p-adaptivity is now possible.\\
However, the algorithm implemented has one major issue. The historic variables, that is, the plastic strain $\pstrain$ and the variables that refer to hardening can only be updated at the end of a newton step when the algorithm has converged. Therefore, those historic variables are stored on tags inside an iteration of a Newton step. Then, a convergence check is done and if the equilibrium is not achieved at the end of the iteration then the algorithm recover the values of the historic variables from the previously converged step.\\
The main problem is the storage of the variables on tags on a background mesh. This issue, we think can be tackled using tools coming from mixed formulations and configurational mechanics. In other terms by using a more sophisticated implementation, it should be possible to avoid the need for historic variables.
\newpage
\label{Numerical_Implementation}
\begin{figure}[h!]
\centering
\includegraphics[width = 140mm]{Figures/algorithm_for_plasticity.pdf}
\caption{The return-mapping algorithm in small strains plasticity }
\label{MoFEM_implementation_2}
\end{figure}
\section{Numerical implementation for some problems}
\subsection{1D problem : beam in traction}
\begin{figure}
\centering
\includegraphics[width = 140mm]{Figures/3D_beam_problem}
\caption{3D beam problem. }
\label{MoFEM_implementation}
\end{figure}
\begin{figure}
\centering
\begin{subfigure}[b]{0.4\linewidth}
\includegraphics[width=\linewidth]{Figures/size-6}
\caption{Meshsize 6}
\label{M6}
\end{subfigure}
\begin{subfigure}[b]{0.4\linewidth}
\includegraphics[width=\linewidth]{Figures/size_7}
\caption{Meshsize 7}
\label{M7}
\end{subfigure}
\begin{subfigure}[b]{0.4\linewidth}
\includegraphics[width=\linewidth]{Figures/size_8}
\caption{Meshsize 8}
\label{M8}
\end{subfigure}
\caption{Mesh with \ref{M6}) 1472 elements \ref{M7}) 302 elements \ref{M8}) 143 elements}
\end{figure}
\begin{figure}
\centering
\includegraphics[width = 150mm]{Figures/graphs_1D/graph_comparison_displacement_force_mesh_density.pdf}
\caption{Load-Displacement path for bar stretching for different mesh sizes (perfect plasticity, displacement control)}
\label{1D_problem}
\end{figure}
\subsection{3D problem : Plate with circular hole in traction}
\begin{figure}
\centering
\includegraphics[width = 100mm]{Figures/stretch_beam_problem}
\caption{Problem the stretched hollow plate for displacement control}
\label{stretch_plate}
\end{figure}
\begin{figure}
\centering
\includegraphics[width = 90mm]{Figures/mesh_visualization_2}
\caption{Representation of the mesh for the 3D problem}
\label{MoFEM_implementation}
\end{figure}
\begin{figure}
\centering
\includegraphics[width = 160mm]{Figures/stress_zz_concentration}
\caption{$\Cstress_{zz}$ in the hollow plate  (pure traction). Young's. modulus : 1, poisson ratio : 0.1, isotropic hardening factor : 0.01, kinematic hardening : 0. , viscosity factor : 1. , yield stress : 1. }
\label{MoFEM_implementation}
\end{figure}


\begin{figure}[h!]
\centering
\includegraphics[width = 150mm]{Figures/HolePlate_compare_meshsize.pdf}
\caption{Load-Displacement path for inhomogeneous problem : hollow plate (perfect plasticity, displacement control) for different element sizes}
\label{Inhomogeneous_problem_mesh}
\end{figure}

\begin{figure}
\centering
\includegraphics[width = 150mm]{Figures/HolePlate_order_compare.pdf}
\caption{Load-Displacement path for inhomogeneous problem : hollow plate (perfect plasticity, displacement control) for different order of approximation }
\label{Inhomogeneous_problem_order}
\end{figure}










