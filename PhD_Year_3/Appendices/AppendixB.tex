% Chapter Template

\chapter{Large strains} % Main chapter title

\label{AppendixB} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}
%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Elasticity in Large strains with classical formulation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Overview of the theory}
A finite hyperelasticity model was implemented in MoFEM. Those models are based on the existence of a free energy scalar function, namely:
\begin{equation}
\begin{array}{ccc}
\psi : \mathscr{T}_2 & \rightarrow  & \mathbb{R} \\
  \ \ \ \ \mathbf{F} & \mapsto & \psi(\mathbf{F})
\end{array}
\end{equation}
where $\mathscr{T}_2 $ is the space of tensors of rank 2, and  $\mathbf{F}$ is the deformation gradient (in the physicla model). The first Piola-Kirchhoff stress can be written as:
\begin{equation}
\mathbf{P} = \mathbf{P}(\mathbf{F}) = \overline{\rho}\frac{\partial \psi}{\partial \mathbf{F}}
\label{const_Piola}
\end{equation}
To simplify, we assume that the stress state depends only on the current deformation state and not on the deformation history. Also pure hyperelastic models are assumed to be non-dissipative so there is no need to introduced internal variables in the model. The constitutive equation for the Kirchhoff stress tensor is defined by:
\begin{equation}
\boldsymbol{\tau} \equiv \mathbf{P}\mathbf{F}^T
\label{const_Kirchhoff}
\end{equation}
Another common axiom in hyperelasticity model is the axiom of material objectivity. Material objectivity means that the free energy function must be invariant under changes in the observer. The mathematical representation of objectivity is:
\begin{equation}
\forall \mathbf{Q} \in \mathscr{O}(\Omega), \ \psi(\mathbf{Q}\mathbf{F}) = \psi(\mathbf{F})
\end{equation}
where $\mathscr{O}(\Omega)$ is the space of orthogonal (rotation) tensors. This aasumption enforces the fact that the free enrgy depends only on the stretch part of the deformation gradient $\mathbf{F}$ in the polar decomposition 
$\mathbf{F} = \mathbf{R}\mathbf{U}$ where $\mathbf{R}$ is an orthonormal tensor and $\mathbf{U}$ is the (right) stretch tensor (which also has the property to be symmetric). Therefore we have:
\begin{equation}
\psi(\mathbf{F}) = \psi(\mathbf{U}) 
\end{equation}
Another assumption of the model is that we will consider isotropic material models. In our mathematical framework, this implies:
\begin{equation}
\forall \mathbf{Q} \in \mathscr{O}(\Omega), \ \psi(\mathbf{F}\mathbf{Q}) = \psi(\mathbf{F})
\end{equation}
If we consider the polar decomposition $\mathbf{F} = \mathbf{V}\mathbf{R}$, where $\mathbf{V}$ is the (left) stretch tensor, the assumption of isotropy implies:
\begin{equation}
\psi(\mathbf{F}) = \psi(\mathbf{V})
\end{equation}
We now introduce the left Cauchy-Green tensor $\mathbf{B} = \mathbf{F}\mathbf{F}^T = \mathbf{V}^2$ and we can write:
\begin{equation}
\tilde{\psi}(\mathbf{B})=\psi \left(\sqrt{\mathbf{B}}\right) = \psi(\mathbf{F})
\end{equation}
If we apply the chain rule:
\begin{equation}
\begin{array}{ccc}
\mathbf{P} = & \overline{\rho}\dfrac{\partial \tilde{\psi}}{\partial \mathbf{B}} : \dfrac{\partial \mathbf{B}}{\partial \mathbf{F}} = & 2\overline{\rho}\dfrac{\partial \tilde{\psi}}{\partial \mathbf{B}}\mathbf{F}, \\
& & \\
\boldsymbol{\tau} = & \tilde{\boldsymbol{\tau}}(\mathbf{B}) = & 2\overline{\rho}\dfrac{\partial \tilde{\psi}}{\partial \mathbf{B}}\mathbf{B}.
\end{array} 
\end{equation}
In order to derive a compressible model, we split the deformation gradient into a volumetric and isochoric part:
\begin{equation}
\mathbf{F} = \mathbf{F}_{\mathrm{iso}}\mathbf{F}_{\mathrm{vol}}
\end{equation}
where $\mathbf{F}_{\mathrm{vol}} \equiv \left(\mathrm{det} \ \mathbf{F}\right)^{\frac{1}{3}} \mathbf{I}$ is the volmetric part of the deformation gradient and $\mathbf{F}_{\mathrm{iso}}  \equiv \left(\mathrm{det} \ \mathbf{F}\right)^{-\frac{1}{3}} \mathbf{F}  $ is the isochoric part of the deformation gradient. Following the previous derivations we define:
\begin{equation}
\mathbf{B}_{\mathrm{iso}} \equiv \mathbf{F}_{\mathrm{iso}}\mathbf{F}_{\mathrm{iso}}^T = \left( \mathrm{det}\mathbf{F}\right)^{-\frac{2}{3}}\mathbf{F}\mathbf{F}^T.
\end{equation}
Then we right the first and second principal invariants:
\begin{equation}
I_1 \equiv \mathrm{tr}\mathbf{B}_{\mathrm{iso}} \  \  \  I_2 \equiv \frac{1}{2}(I_1)^2 - \mathrm{tr}\left[\mathbf{B}_{\mathrm{iso}}^2\right]
\end{equation}
Now we can give an explicit formula for the compressible version of the Mooney-Rivlin model:
\begin{equation}
\psi(I_1, I_2, J) = C_1(I_1 -3) + C_2(I_2 - 3) + \frac{1}{2}K[\mathrm{ln}(J)]^2
\label{material}
\end{equation}
where $C_1$ and $C_2$ are material constants, $K$ is the bulk modulus and $J \equiv \mathrm{det}\mathbf{F}$ is the volume ratio. The so-called Neo-Hookean model can be found by setting $C_2 = 0$ in \eqref{material}.\\
By following the equations \eqref{const_Piola} and \eqref{const_Kirchhoff} we can give an explicit expression for $\boldsymbol{\tau}$ :
\begin{equation}
\boldsymbol{\tau} = 2\left(C_1 + C_2 I_1 \right)\mathrm{dev}\left[\mathbf{B}_{\mathrm{iso}}\right] - 2C_2\mathrm{dev}\left[\mathbf{B}_{\mathrm{iso}}^2\right] + K (\mathrm{ln} \ J)\mathbf{I}
\end{equation}  
where $\mathrm{dev}(\cdot)$ is the operator returning the deviatoric part of a tensor of rank 2 i.e:
\begin{equation}
\begin{array}{cccl}
\mathrm{dev} :& \mathscr{T}_2 &\rightarrow &\mathscr{T}_2 \\
& \mathbf{A} & \mapsto & \mathrm{dev}(\mathbf{A}) = \mathbf{A}- \frac{1}{3}\mathrm{tr}(\mathbf{A})\mathbf{I} 
\end{array}
\end{equation}
Now what is left is to linearise the problem and give an expression for tangent stiffness matrix. The first law of thermodynamics gives us an integral formulation  of the elasticity problem. Therefore, we have, in a spatial description (neglecting the contribution of inertia forces):
\begin{equation}
\intOmt{\mathrm{div}\boldsymbol{\sigma} \cdot \delta \mathbf{u}} + \intOm{\mathbf{b} \cdot \delta \mathbf{u}} = 0 \ \fa \delta \mathbf{u} \in \mathbf{H}^1(\Om) \, ,
\label{First_law}
\end{equation}
where, $\mathbf{b}$ represent the body forces applied on the system (e.g gravity force) and $\boldsymbol{\sigma}$ is the Cauchy stress defined by the classical formula:
\begin{equation}
\boldsymbol{\sigma} = J^{-1}\mathbf{P}\mathbf{F}^T \, .
\end{equation}
By applying Gauss divergence theorem to \eqref{First_law} we have:
\begin{equation}
\intOmt{\left(\boldsymbol{\sigma} : \nabla \delta \mathbf{u} - \mathbf{b} \cdot \delta \mathbf{u}\right)} - \intGat{\boldsymbol{\sigma}\mathbf{n} \cdot \delta \mathbf{u}} = 0 \, ,
\end{equation}
where $\mathbf{n}$ is the outward unit normal vector on $\partial \Omega$, the border of the domain $\Omega$.  Remembering that $\boldsymbol{\sigma}\mathbf{n} = \mathbf{t}$ where $\mathbf{t}$ is the traction vector, we therefore consider the derivative of the functional:
\begin{equation}
\Pi(\boldsymbol{\eta}) = \intOmt{\left(\boldsymbol{\sigma} : \nabla_x \boldsymbol{\eta}- \mathbf{b} \cdot \boldsymbol{\eta} \right)} - \intGat{\mathbf{t} \cdot \boldsymbol{\eta}} \, , 
\end{equation}
where $\nabla_x $ indicates that the gradient is taken according to the spatial configuration. For the elastic constitutive model, we have:
\begin{equation}
\boldsymbol{\sigma} = \boldsymbol{\sigma}(\mathbf{F}) = \boldsymbol{\sigma}(\mathbf{I} + \nabla_X \mathbf{u})
\end{equation}
By deriving the differential of $\Pi$ along $\mathbf{u}$ we have:
\begin{equation}
\mathrm{D}\Pi(\overline{\mathbf{u}}, \boldsymbol{\eta})[\delta \mathbf{u}] = \left.\frac{\mathrm{d}}{\mathrm{d}\epsilon}\right|_{\epsilon = 0}\left[\intOmt{\left(\boldsymbol{\sigma}(\mathbf{F}(\epsilon)) : \nabla_x \boldsymbol{\eta}- \mathbf{b} \cdot \boldsymbol{\eta} \right)} - \intGat{\mathbf{t} \cdot \boldsymbol{\eta}} \,\right] \, ,
\end{equation}
where $\mathbf{F}(\epsilon) = \mathbf{I} + \nabla_X(\overline{\mathbf{u}} + \epsilon \delta \mathbf{u}) = \mathbf{F}_{\overline{\mathbf{u}}} + \epsilon \nabla_X \delta \mathbf{u} $.  Using a Taylor expansion to approximate the Cauchy stress we have:
\begin{equation}
\boldsymbol{\sigma}(\mathbf{F}(\epsilon)) \approx \mathbf{F}_{\overline{\mathbf{u}}} + \epsilon \left.\frac{\partial \boldsymbol{\sigma}}{\partial \mathbf{F}}\right|_{\mathbf{F}_{\overline{\mathbf{u}}}} : \nabla_X \delta \mathbf{u}
\end{equation}
By exploiting the formula $\nabla_x \mathbf{a} = \nabla_X \mathbf{a}\mathbf{F}^{-1}$ and the change of variable between the initial and current configuration $\intOmt{\mathbf{a}(\mathbf{x})} = \intOm{J \, \mathbf{a}(\mathbf{x}({\mathbf{X})})}$ we have:
\begin{equation}
\mathrm{D}\Pi(\overline{\mathbf{u}}, \boldsymbol{\eta})[\delta \mathbf{u}] = \intOmt{\mathbb{K} : \nabla_x \delta \mathbf{u} : \nabla_x \boldsymbol{\eta}}
\end{equation}
where $\mathbb{K}$ is the stiffness matrix whose components are given by:
\begin{equation}
\mathbb{K}_{ijkl} = \frac{1}{J}\frac{\partial P_{im}}{\partial F_{kn}}F_{jm}F_{ln}
\end{equation}
by using the formula:
\begin{equation}
\mathbf{P} = \boldsymbol{\tau}\mathbf{F}^{-T} 
\end{equation}
along with the chaine rule we have:
\begin{equation}
\mathbb{K}_{ijkl}  = \frac{1}{J}\frac{\partial \tau_{ij}}{\partial F_{km}}F_{lm}-\sigma_{il}\delta_{jk} = \frac{2}{J}\frac{\partial \tau_{ij}}{\partial B_{km}}B_{ml}-\sigma_{il}\delta_{jk} 
\end{equation}
For completeness we now give an expression for $\frac{\partial \boldsymbol{\tau}}{\partial \mathbf{B}}$ for the compressible Mooney-Rivlin model:
\begin{equation}
\begin{array}{lc}
\dfrac{\partial\boldsymbol{\tau}}{\partial \mathbf{B}} = & \\ 
  & 2[C_1 + C_2 \mathrm{tr}(\mathbf{B}_{\mathrm{iso}})]J^{-\frac{2}{3}}\mathbb{I}^{\mathrm{dev}}
 -\dfrac{2}{3}[C_1 + C_2 \mathrm{tr}(\mathbf{B}_{\mathrm{iso}})]\mathrm{dev}(\mathbf{B}_{\mathrm{iso}}) \otimes \mathbf{B}^{-1}
  + \dfrac{1}{2}K \mathbf{I}\otimes\mathbf{B}^{-1} \\
 & + 2C_2\left[J^{-\frac{2}{3}}\mathbf{I}\otimes\mathbf{B}^{-1} 
 -\dfrac{1}{3}\mathrm{tr}(\mathbf{B}_{\mathrm{iso}})\mathbf{B}^{-1} \otimes \mathrm{dev}(\mathbf{B}_{\mathrm{iso}}) 
+ \mathbb{I}^{\mathrm{dev}} : \dfrac{\partial \mathbf{B}_{\mathrm{iso}}^{2}}{\partial \mathbf{B}_{\mathrm{iso}}} : \dfrac{\partial \mathbf{B}_{\mathrm{iso}}}{\partial \mathbf{B}}\right] 
\end{array}
\end{equation}
where $\mathbb{I}^{\mathrm{dev}}$ is the operator that projects every tensor of rank 2 on its deviatoric part:
\begin{equation}
\mathbb{I}^{\mathrm{dev}} = \mathbb{I} - \frac{1}{3}\mathbf{I} \otimes \mathbf{I}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Possible applications}
Now that the classical approach for solving the elasticity problem at large strains has been implemented in MoFEM, it will be used to compare results obtained with the classical approach and those obtained with the mixed approach (coupled with the concepts arising in configurational mechanics) that will be developed in the following sections. Indeed, as the free energy changes from one application to another, the formulae given in previous sections will be changewd accordingly.