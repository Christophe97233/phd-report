\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {section}{\numberline {1}Motivations}{2}
\contentsline {section}{\numberline {2}Elasticity in Large strains with classical formulation}{3}
\contentsline {subsection}{\numberline {2.1}Overview of the theory}{3}
\contentsline {subsection}{\numberline {2.2}Possible applications}{7}
\contentsline {section}{\numberline {3}Elasticity in Large strains with mixed formulation }{7}
\contentsline {subsection}{\numberline {3.1}Theory of mixed elements applied for elasticity problems (small strains)}{7}
\contentsline {subsection}{\numberline {3.2}Extension to large strains}{9}
\contentsline {subsection}{\numberline {3.3}Body}{10}
\contentsline {subsection}{\numberline {3.4}Kinematics}{11}
\contentsline {subsection}{\numberline {3.5}Equilibrium}{12}
\contentsline {subsubsection}{\numberline {3.5.1}Equations in weak form: Spatial problem}{13}
\contentsline {paragraph}{Kinematics}{13}
\contentsline {paragraph}{Consistency equation}{14}
\contentsline {paragraph}{Physical equation}{15}
\contentsline {paragraph}{Angular momentum equation}{15}
\contentsline {paragraph}{Linear momentum equation}{16}
\contentsline {paragraph}{Element equations}{16}
\contentsline {paragraph}{Spaces}{16}
\contentsline {subsubsection}{\numberline {3.5.2}Equations in strong form}{16}
\contentsline {paragraph}{Approximation of logarithmic stretch}{17}
\contentsline {subsection}{\numberline {3.6}Practical Construction of local polynomial base for H-div space}{17}
\contentsline {subsection}{\numberline {3.7}Comparison with results in classical formulation}{18}
\contentsline {subsubsection}{\numberline {3.7.1}Cook problem}{18}
\contentsline {subsubsection}{\numberline {3.7.2}Column Bending}{20}
\contentsline {subsubsection}{\numberline {3.7.3}Torsion of a beam with a square cross section}{21}
\contentsline {subsection}{\numberline {3.8}Extension to plastic problems}{22}
\contentsline {subsubsection}{\numberline {3.8.1}Equilibrium}{22}
\contentsline {subsubsection}{\numberline {3.8.2}Constraints}{24}
\contentsline {subsubsection}{\numberline {3.8.3}Dissipation}{25}
\contentsline {subsubsection}{\numberline {3.8.4}Equations in strong form}{26}
\contentsline {section}{\numberline {4}Conclusion and Future Work}{26}
\contentsline {subsection}{\numberline {4.1}Conclusion}{26}
\contentsline {subsection}{\numberline {4.2}Future work}{27}
\contentsline {section}{\numberline {A}Schur complement}{28}
