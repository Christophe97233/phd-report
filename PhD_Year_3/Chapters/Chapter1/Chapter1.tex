% Chapter Template

\chapter{Mixed Finite Formulation Theory} % Main chapter title

\label{Chapter1} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}
 % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}
%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Elasticity in Large strains with classical formulation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Overview of the theory}
A finite hyperelasticity model was implemented in MoFEM. Those models are based on the existence of a free energy scalar function, namely:
\begin{equation}
\begin{array}{ccc}
\psi : \mathscr{T}_2 & \rightarrow  & \mathbb{R} \\
  \ \ \ \ \mathbf{F} & \mapsto & \psi(\mathbf{F})
\end{array}
\end{equation}
where $\mathscr{T}_2 $ is the space of tensors of rank 2, and  $\mathbf{F}$ is the deformation gradient (in the physicla model). The first Piola-Kirchhoff stress can be written as:
\begin{equation}
\mathbf{P} = \mathbf{P}(\mathbf{F}) = \overline{\rho}\frac{\partial \psi}{\partial \mathbf{F}}
\label{const_Piola}
\end{equation}
To simplify, we assume that the stress state depends only on the current deformation state and not on the deformation history. Also pure hyperelastic models are assumed to be non-dissipative so there is no need to introduced internal variables in the model. The constitutive equation for the Kirchhoff stress tensor is defined by:
\begin{equation}
\boldsymbol{\tau} \equiv \mathbf{P}\mathbf{F}^T
\label{const_Kirchhoff}
\end{equation}
Another common axiom in hyperelasticity model is the axiom of material objectivity. Material objectivity means that the free energy function must be invariant under changes in the observer. The mathematical representation of objectivity is:
\begin{equation}
\forall \mathbf{Q} \in \mathscr{O}(\Omega), \ \psi(\mathbf{Q}\mathbf{F}) = \psi(\mathbf{F})
\end{equation}
where $\mathscr{O}(\Omega)$ is the space of orthogonal (rotation) tensors. This aasumption enforces the fact that the free enrgy depends only on the stretch part of the deformation gradient $\mathbf{F}$ in the polar decomposition 
$\mathbf{F} = \mathbf{R}\mathbf{U}$ where $\mathbf{R}$ is an orthonormal tensor and $\mathbf{U}$ is the (right) stretch tensor (which also has the property to be symmetric). Therefore we have:
\begin{equation}
\psi(\mathbf{F}) = \psi(\mathbf{U}) 
\end{equation}
Another assumption of the model is that we will consider isotropic material models. In our mathematical framework, this implies:
\begin{equation}
\forall \mathbf{Q} \in \mathscr{O}(\Omega), \ \psi(\mathbf{F}\mathbf{Q}) = \psi(\mathbf{F})
\end{equation}
If we consider the polar decomposition $\mathbf{F} = \mathbf{V}\mathbf{R}$, where $\mathbf{V}$ is the (left) stretch tensor, the assumption of isotropy implies:
\begin{equation}
\psi(\mathbf{F}) = \psi(\mathbf{V})
\end{equation}
We now introduce the left Cauchy-Green tensor $\mathbf{B} = \mathbf{F}\mathbf{F}^T = \mathbf{V}^2$ and we can write:
\begin{equation}
\tilde{\psi}(\mathbf{B})=\psi \left(\sqrt{\mathbf{B}}\right) = \psi(\mathbf{F})
\end{equation}
If we apply the chain rule:
\begin{equation}
\begin{array}{ccc}
\mathbf{P} = & \overline{\rho}\dfrac{\partial \tilde{\psi}}{\partial \mathbf{B}} : \dfrac{\partial \mathbf{B}}{\partial \mathbf{F}} = & 2\overline{\rho}\dfrac{\partial \tilde{\psi}}{\partial \mathbf{B}}\mathbf{F}, \\
& & \\
\boldsymbol{\tau} = & \tilde{\boldsymbol{\tau}}(\mathbf{B}) = & 2\overline{\rho}\dfrac{\partial \tilde{\psi}}{\partial \mathbf{B}}\mathbf{B}.
\end{array} 
\end{equation}
In order to derive a compressible model, we split the deformation gradient into a volumetric and isochoric part:
\begin{equation}
\mathbf{F} = \mathbf{F}_{\mathrm{iso}}\mathbf{F}_{\mathrm{vol}}
\end{equation}
where $\mathbf{F}_{\mathrm{vol}} \equiv \left(\mathrm{det} \ \mathbf{F}\right)^{\frac{1}{3}} \mathbf{I}$ is the volmetric part of the deformation gradient and $\mathbf{F}_{\mathrm{iso}}  \equiv \left(\mathrm{det} \ \mathbf{F}\right)^{-\frac{1}{3}} \mathbf{F}  $ is the isochoric part of the deformation gradient. Following the previous derivations we define:
\begin{equation}
\mathbf{B}_{\mathrm{iso}} \equiv \mathbf{F}_{\mathrm{iso}}\mathbf{F}_{\mathrm{iso}}^T = \left( \mathrm{det}\mathbf{F}\right)^{-\frac{2}{3}}\mathbf{F}\mathbf{F}^T.
\end{equation}
Then we right the first and second principal invariants:
\begin{equation}
I_1 \equiv \mathrm{tr}\mathbf{B}_{\mathrm{iso}} \  \  \  I_2 \equiv \frac{1}{2}(I_1)^2 - \mathrm{tr}\left[\mathbf{B}_{\mathrm{iso}}^2\right]
\end{equation}
Now we can give an explicit formula for the compressible version of the Mooney-Rivlin model:
\begin{equation}
\psi(I_1, I_2, J) = C_1(I_1 -3) + C_2(I_2 - 3) + \frac{1}{2}K[\mathrm{ln}(J)]^2
\label{material}
\end{equation}
where $C_1$ and $C_2$ are material constants, $K$ is the bulk modulus and $J \equiv \mathrm{det}\mathbf{F}$ is the volume ratio. The so-called Neo-Hookean model can be found by setting $C_2 = 0$ in \eqref{material}.\\
By following the equations \eqref{const_Piola} and \eqref{const_Kirchhoff} we can give an explicit expression for $\boldsymbol{\tau}$ :
\begin{equation}
\boldsymbol{\tau} = 2\left(C_1 + C_2 I_1 \right)\mathrm{dev}\left[\mathbf{B}_{\mathrm{iso}}\right] - 2C_2\mathrm{dev}\left[\mathbf{B}_{\mathrm{iso}}^2\right] + K (\mathrm{ln} \ J)\mathbf{I}
\end{equation}  
where $\mathrm{dev}(\cdot)$ is the operator returning the deviatoric part of a tensor of rank 2 i.e:
\begin{equation}
\begin{array}{cccl}
\mathrm{dev} :& \mathscr{T}_2 &\rightarrow &\mathscr{T}_2 \\
& \mathbf{A} & \mapsto & \mathrm{dev}(\mathbf{A}) = \mathbf{A}- \frac{1}{3}\mathrm{tr}(\mathbf{A})\mathbf{I} 
\end{array}
\end{equation}
Now what is left is to linearise the problem and give an expression for tangent stiffness matrix. The first law of thermodynamics gives us an integral formulation  of the elasticity problem. Therefore, we have, in a spatial description (neglecting the contribution of inertia forces):
\begin{equation}
\intOmt{\mathrm{div}\boldsymbol{\sigma} \cdot \delta \mathbf{u}} + \intOm{\mathbf{b} \cdot \delta \mathbf{u}} = 0 \ \fa \delta \mathbf{u} \in \mathbf{H}^1(\Om) \, ,
\label{First_law}
\end{equation}
where, $\mathbf{b}$ represent the body forces applied on the system (e.g gravity force) and $\boldsymbol{\sigma}$ is the Cauchy stress defined by the classical formula:
\begin{equation}
\boldsymbol{\sigma} = J^{-1}\mathbf{P}\mathbf{F}^T \, .
\end{equation}
By applying Gauss divergence theorem to \eqref{First_law} we have:
\begin{equation}
\intOmt{\left(\boldsymbol{\sigma} : \nabla \delta \mathbf{u} - \mathbf{b} \cdot \delta \mathbf{u}\right)} - \intGat{\boldsymbol{\sigma}\mathbf{n} \cdot \delta \mathbf{u}} = 0 \, ,
\end{equation}
where $\mathbf{n}$ is the outward unit normal vector on $\partial \Omega$, the border of the domain $\Omega$.  Remembering that $\boldsymbol{\sigma}\mathbf{n} = \mathbf{t}$ where $\mathbf{t}$ is the traction vector, we therefore consider the derivative of the functional:
\begin{equation}
\Pi(\boldsymbol{\eta}) = \intOmt{\left(\boldsymbol{\sigma} : \nabla_x \boldsymbol{\eta}- \mathbf{b} \cdot \boldsymbol{\eta} \right)} - \intGat{\mathbf{t} \cdot \boldsymbol{\eta}} \, , 
\end{equation}
where $\nabla_x $ indicates that the gradient is taken according to the spatial configuration. For the elastic constitutive model, we have:
\begin{equation}
\boldsymbol{\sigma} = \boldsymbol{\sigma}(\mathbf{F}) = \boldsymbol{\sigma}(\mathbf{I} + \nabla_X \mathbf{u})
\end{equation}
By deriving the differential of $\Pi$ along $\mathbf{u}$ we have:
\begin{equation}
\mathrm{D}\Pi(\overline{\mathbf{u}}, \boldsymbol{\eta})[\delta \mathbf{u}] = \left.\frac{\mathrm{d}}{\mathrm{d}\epsilon}\right|_{\epsilon = 0}\left[\intOmt{\left(\boldsymbol{\sigma}(\mathbf{F}(\epsilon)) : \nabla_x \boldsymbol{\eta}- \mathbf{b} \cdot \boldsymbol{\eta} \right)} - \intGat{\mathbf{t} \cdot \boldsymbol{\eta}} \,\right] \, ,
\end{equation}
where $\mathbf{F}(\epsilon) = \mathbf{I} + \nabla_X(\overline{\mathbf{u}} + \epsilon \delta \mathbf{u}) = \mathbf{F}_{\overline{\mathbf{u}}} + \epsilon \nabla_X \delta \mathbf{u} $.  Using a Taylor expansion to approximate the Cauchy stress we have:
\begin{equation}
\boldsymbol{\sigma}(\mathbf{F}(\epsilon)) \approx \mathbf{F}_{\overline{\mathbf{u}}} + \epsilon \left.\frac{\partial \boldsymbol{\sigma}}{\partial \mathbf{F}}\right|_{\mathbf{F}_{\overline{\mathbf{u}}}} : \nabla_X \delta \mathbf{u}
\end{equation}
By exploiting the formula $\nabla_x \mathbf{a} = \nabla_X \mathbf{a}\mathbf{F}^{-1}$ and the change of variable between the initial and current configuration $\intOmt{\mathbf{a}(\mathbf{x})} = \intOm{J \, \mathbf{a}(\mathbf{x}({\mathbf{X})})}$ we have:
\begin{equation}
\mathrm{D}\Pi(\overline{\mathbf{u}}, \boldsymbol{\eta})[\delta \mathbf{u}] = \intOmt{\mathbb{K} : \nabla_x \delta \mathbf{u} : \nabla_x \boldsymbol{\eta}}
\end{equation}
where $\mathbb{K}$ is the stiffness matrix whose components are given by:
\begin{equation}
\mathbb{K}_{ijkl} = \frac{1}{J}\frac{\partial P_{im}}{\partial F_{kn}}F_{jm}F_{ln}
\end{equation}
by using the formula:
\begin{equation}
\mathbf{P} = \boldsymbol{\tau}\mathbf{F}^{-T} 
\end{equation}
along with the chaine rule we have:
\begin{equation}
\mathbb{K}_{ijkl}  = \frac{1}{J}\frac{\partial \tau_{ij}}{\partial F_{km}}F_{lm}-\sigma_{il}\delta_{jk} = \frac{2}{J}\frac{\partial \tau_{ij}}{\partial B_{km}}B_{ml}-\sigma_{il}\delta_{jk} 
\end{equation}
For completeness we now give an expression for $\frac{\partial \boldsymbol{\tau}}{\partial \mathbf{B}}$ for the compressible Mooney-Rivlin model:
\begin{equation}
\begin{array}{lc}
\dfrac{\partial\boldsymbol{\tau}}{\partial \mathbf{B}} = & \\ 
  & 2[C_1 + C_2 \mathrm{tr}(\mathbf{B}_{\mathrm{iso}})]J^{-\frac{2}{3}}\mathbb{I}^{\mathrm{dev}}
 -\dfrac{2}{3}[C_1 + C_2 \mathrm{tr}(\mathbf{B}_{\mathrm{iso}})]\mathrm{dev}(\mathbf{B}_{\mathrm{iso}}) \otimes \mathbf{B}^{-1}
  + \dfrac{1}{2}K \mathbf{I}\otimes\mathbf{B}^{-1} \\
 & + 2C_2\left[J^{-\frac{2}{3}}\mathbf{I}\otimes\mathbf{B}^{-1} 
 -\dfrac{1}{3}\mathrm{tr}(\mathbf{B}_{\mathrm{iso}})\mathbf{B}^{-1} \otimes \mathrm{dev}(\mathbf{B}_{\mathrm{iso}}) 
+ \mathbb{I}^{\mathrm{dev}} : \dfrac{\partial \mathbf{B}_{\mathrm{iso}}^{2}}{\partial \mathbf{B}_{\mathrm{iso}}} : \dfrac{\partial \mathbf{B}_{\mathrm{iso}}}{\partial \mathbf{B}}\right] 
\end{array}
\end{equation}
where $\mathbb{I}^{\mathrm{dev}}$ is the operator that projects every tensor of rank 2 on its deviatoric part:
\begin{equation}
\mathbb{I}^{\mathrm{dev}} = \mathbb{I} - \frac{1}{3}\mathbf{I} \otimes \mathbf{I}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Possible applications}
Now that the classical approach for solving the elasticity problem at large strains has been implemented in MoFEM, it will be used to compare results obtained with the classical approach and those obtained with the mixed approach (coupled with the concepts arising in configurational mechanics) that will be developed in the following sections. Indeed, as the free energy changes from one application to another, the formulae given in previous sections will be changewd accordingly.
%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Motivation for Mixed Formulation}
%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Results from functional analysis}

\begin{theo}
Let X, Y be Banach spaces and let $A : X \rightarrow Y$ be bounded and linear. A is bounded below if and only if A is injective and the range of A is closed.
\end{theo}
Proof. Suppose that $A$ is bounded below and that $x_{n} \in X$ with \[A x_{n} \rightarrow y \in Y \, .\]
Then $\left\|x_{n}-x_{m}\right\| \leq c\left\|A x_{n}-A x_{m}\right\| \rightarrow 0\) as \(n, m \rightarrow \infty$ ; the sequence $\left(x_{n}\right)_{n \geq 1}$ is
Cauchy. By completeness $x_{n} \rightarrow x \in X$ and so $A x_{n} \rightarrow A x$ which makes $y=A x$.
Therefore $y$ belongs to the range of $A$.
Conversely, suppose that $A : X \rightarrow \operatorname{Im}(A)$ is invertible. since $\operatorname{Im}(A)$ is a
Banach space, we can use the bounded inverse theorem

$$
\|x\|=\left\|A^{-1} A x\right\| \leq c\|A x\|_{\operatorname{Im}(A)}=c\|A x\|_{Y}
$$

\begin{theo}[Closed Range theorem]
Let $A : V \rightarrow W$ be a bounded linear operator and $A'$ its dual operator. The following statements are equivalent:
\begin{itemize}
\item $R(A)$ is closed in W
\item $R(A')$ is closed in V'
\item $R(A) = (\operatorname{Ker}(A'))^{\perp} := \setbra{w \in W \left| \lan{l , w} = 0 \fa l \in \operatorname{Ker}(A') \right.}$ 
\item $R(A') = (\operatorname{Ker}(A))^{\perp} := \setbra{v \in V' \left| \lan{v , u} = 0 \fa u \in \operatorname{Ker}(A) \right.}$ 
\end{itemize}
\end{theo}
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Small strain elasticity}

 \parencite{Guzman2010}

%-----------------------------------
%	SUBSECTION 2.1
%-----------------------------------
\subsection{Definitions from functional analysis}

\begin{equation}
\mathrm{H}(\mathrm{div}; \Om) := \setbra{\left. \boldsymbol{\tau} \in \Ltwon \right| \nabla \cdot \boldsymbol{\tau} \in \Ltwo }
\end{equation}
where $\nabla \cdot \boldsymbol{\tau} $ has to be understood in the distributional sense, i.e, there exists $z \in \Ltwo$ such that:
\begin{equation}
-\intOm{\nabla\phi \cdot \boldsymbol{\tau}} = \intOm{z\phi} \, , \fa \phi \in \CinfoO
\end{equation}
\begin{theo}
Let $\Om$ be a bounded domain of $\Ren$ with Lipschitz-continuous boundary $\Ga$. Then there exist a linear bounded surjective operator $\ga_n : \mathrm{H}(\mathrm{div}; \Om) \rightarrow \mathrm{H}^{-\frac{1}{2}}(\Ga)$ such that for each $\boldsymbol{\tau} \in \sqbra{H^1(\Om)}^n$, $\ga_n(\boldsymbol{\tau})$ is identified through the inner product of $L^2(\Ga)$, with $\ga_0(\boldsymbol{\tau})\cdot \mathbf{n}$
\end{theo}
Recall,
\begin{equation}
\norm{\xi}_{\frac{1}{2}, \Ga} := \inf \setbra{\left. \norm{v}_{1,\Om} \right| v \in H^1(\Om) \ \mathrm{such \ that \ } \ga_0(v)=\xi} \fa \xi \in \Hhalf
\end{equation}
\begin{theo}[Green's Identity in $\Hdiv$]
Let $\Om$ be a bounded domain of $\Ren$ with Lipschitz-continuous boundary $\Ga$. Then there holds:
\begin{equation}
\lan{\ga_n(\boldsymbol{\tau}), \, \ga_0(w)} = \intOm{\boldsymbol{\tau} \cdot \nabla w} + \intOm{w \nabla \cdot \boldsymbol{\tau}} \ \fa w \in \mathrm{H}^1(\Om), \ \fa \boldsymbol{\tau} \in \Hdiv
\end{equation}
\end{theo}
As a consequence we have the following result:
\begin{theo}
Let $\Om$ be a bounded domain of $\Ren$ with Lipschitz-continuous boundary $\Ga$ and let us define:
\begin{equation}
\mathrm{H}^1_{\Delta}(\Om) := \setbra{v \in \mathrm{H}^1(\Om) : \Delta v \in \Ltwo} \, ,
\end{equation}
then there exists a linear and bounded operator $\ga_1 : \mathrm{H}^1_{\Delta}(\Om) \rightarrow \Hdualhalf $ sucht that for each $u \in \mathrm(H)^2(\Om)$, $\ga_1(u)$ is identified, by means of the inner product of $L^2(\Ga)$, with $\ga_0(\nabla u) \cdot \mathbf{n}$, that is,
\begin{equation}
\lan{\ga_1(u), \xi} = \lan{\ga_0(\nabla u), \xi}_{0, \Ga} \ \fa \xi \in \Hhalf , \ \fa u \in \mathrm{H}^2(\Om) 
\end{equation}
Moreover, there holds:
\begin{equation}
\lan{\ga_1(u), \, \ga_0(w)} = \intOm{\nabla u \cdot \nabla w} + \intOm{w \Delta u}, \, \fa w \in \mathrm{H}^1(\Om), \, \fa u \in \mathrm{H}^1_{\Delta}(\Om)  
\end{equation}
\end{theo}
%-----------------------------------
%	SUBSECTION 2.2
%-----------------------------------

\subsection{Classical form of the mixed problem}
Let $(H, \, \lan{\cdot \, , \, \cdot}_H)$ and $(Q, \, \lan{\cdot \, , \, \cdot}_Q)$ be real Hilbert spaces and let $a : H \times H \rightarrow \R$ and $b : H \times Q \rightarrow \R$ be bounded bilinear forms Then $F \in H'$ and $G \in Q'$, we are interested in the following problem: 
\begin{pro}[Classical Mixed Formulation]
Find $(\sigma, \, u) \in H \times Q$. such that:
\begin{equation}
\left\{
\begin{array}{r c l}
%\begin{aligned}
a(\sigma, \, \tau) + b(\tau, \, u) & =  & F(\tau), \, \fa \tau \in H \\
b(\sigma, \, \nu) & = & G(\nu), \, \fa \nu \in Q, 
%\end{aligned}
\end{array}
\right.
\label{Classical_mixed_problem}
\end{equation}
\end{pro}
The bilinear forms $a(\cdot , \, \cdot )$ and $b(\cdot , \, \cdot )$ induce linear forms denoted as $\mathbf{A}$ and $\mathbf{B}$ respectively such that:
\begin{equation}
\begin{array}{r c l}
%\begin{aligned}
\mathbf{A} : H &\longrightarrow &H \\
\sigma &\mapsto& \mathbf{A}(\sigma)
%\end{aligned}
\end{array}
\end{equation}
such that:
\[
a(\sigma, \, \tau) = \lan{\mathbf{A}(\sigma), \, \tau}_H, \, \fa (\sigma, \, \tau) \in H \times H
\]
and similarly:
\begin{equation}
\begin{array}{r c l}
%\begin{aligned}
\mathbf{B} : H &\longrightarrow &Q\\
\sigma &\mapsto& \mathbf{B}(\sigma)
%\end{aligned}
\end{array}
\end{equation}
such that:
\[
b(\sigma , \, u) = \lan{\mathbf{B}(\sigma), \, u }_Q = \lan{\mathbf{B}^*(u), \, \sigma}_H, \, \fa (\sigma, \, u) \in H \times Q
\]
where $\mathbf{B}^*$ is the adjoint operator of $\mathbf{B}$. Using those notations the problem \eqref{Classical_mixed_problem} can be reformulated equivalently as:
\begin{pro}
Find $(\sigma, \, u) \in H \times Q$. such that:
\begin{equation}
\left(
\begin{array}{ll}
\mathbf{A} & \mathbf{B}^* \\
\mathbf{B} & 0
\end{array}
\right)
\left(
\begin{array}{l}
\sigma \\
u
\end{array}
\right)
=
\left(
\begin{array}{l}
\mathcal{R}_H(F)\\
\mathcal{R}_Q(G)
\end{array}
\right), 
\label{matrix_representation}
\end{equation}
where $\mathcal{R}_H$ and $\mathcal{R}_Q$ are the Riesz operators for the Hilbert spaces H and Q respectively.
\end{pro}
%-----------------------------------
%	SUBSECTION 2.3
%-----------------------------------
\subsection{Stability results}
\begin{dfn}[Inf-sup condition]
We say that the bilinear form $b : H \times Q \rightarrow \mathbb{R}$ satusfies the inf-sup (or Ladyzhenskaya-Babuska-Brezzi (LBB) or simply Babuska-Brezzi) condition if there exists a constant $\beta > 0$ such that
\begin{equation}
\sup_{\tau \in H, \, \tau \neq 0} \frac{b(\tau, \,  \nu)}{\norm{\tau}_H} \geq \beta \norm{\nu}_Q, \, \fa \nu \in Q
\end{equation} 
\label{LBB}
\end{dfn}
The following lemma gives a convenient way to characterise the LBB condition (see definition \ref{LBB}). It is a direct consequence of the Closed Range Theorem:
\begin{lem}
The following assertions are equivalent:
\begin{enumerate}[label=(\roman*)]
\item $b(\cdot, \, \cdot)$  satisfies  the LBB condition,
\item $\mathbf{B}^*$  is  an isomorphism from  Q  to  $\mathrm{Ker}(\mathbf{B})^{\perp}$  and  $\norm{\mathbf{B}^*(\nu)}_H \geq \beta \norm{\nu}_Q, \, \fa \nu \in Q$,
\item$\mathbf{B}$  is  an isomorphism from  $\mathrm{Ker}(\mathbf{B})^{\perp}$  to Q and  $\norm{\mathbf{B}(\tau)}_Q \geq \beta \norm{\tau}_H, \, \fa \tau \in H$,
\item $\mathbf{B} : H \rightarrow Q$ is surjective
\end{enumerate}
\end{lem}
Now, the following theorem is fundamental to ensure the well-posedness of the problem \ref{Classical_mixed_problem}
\begin{theo}
Let $V := \mathrm{Ker}(\mathbf{B})$ and let $\Pi : H \rightarrow V$ be the orthogonal projection operator. Assume that:
\begin{enumerate}[label=(\roman*)]
\item $\Pi \mathbf{A} : V \rightarrow V$ is a bijection
\item The bilinear form $b(\cdot , \, \cdot)$ satisfies the LBB condition
\end{enumerate}
Then for each pair $(F, \, G) \in H' \times Q'$ there exists a unique $(\sigma, u) \in H \times Q$ solution of \ref{Classical_mixed_problem}. Moreover there exists a constant $C > 0$, which depends on $\norm{\mathbf{A}}$, $\norm{(\Pi \mathbf{A})^{-1}}$, and $\beta$, such that:
\begin{equation}
\norm{(\sigma, \, u)}_{H \times Q} \leq C \paren{\norm{F}_{H'} + \norm{G}_{Q'}}
\end{equation}
\end{theo}
A less general but very important result (which is actually a corollary of the previous one) is the following:
\begin{theo}
Let $V := \mathrm{Ker}(\mathbf{B})$ and assume that:
\begin{enumerate}[label=(\roman*)]
\item The bilinear form $a(\cdot , \, \cdot)$ is V-elliptic
\item The bilinear form $b(\cdot , \, \cdot)$ satisfies the LBB condition
\end{enumerate}
Then for each pair $(F, \, G) \in H' \times Q'$ there exists a unique $(\sigma, u) \in H \times Q$ solution of \ref{Classical_mixed_problem}. Moreover there exists a constant $C > 0$, which depends on $\norm{\mathbf{A}}$, $\al$, and $\beta$, such that:
\begin{equation}
\norm{(\sigma, \, u)}_{H \times Q} \leq C \paren{\norm{F}_{H'} + \norm{G}_{Q'}}
\end{equation}
where $\al$ is the ellipticity constant.
\end{theo}
%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Discretisation}
%-----------------------------------
%	SUBSECTION 3.1
%-----------------------------------
\subsection{Galerkin scheme}
Let $\setbra{H_h}_{h>0}$ and $\setbra{Q_h}_{h>0}$ be sequences of finite dimensional subspaces of H and Q respectively. if the functions $F \in H'$ and $G \in Q'$ are given, then we have the Galerkin scheme:
Find $(\sigma_h , u_h) \in H_h \times Q_h$ such that:
\begin{equation}
\left\{
\begin{array}{r c l}
%\begin{aligned}
a(\sigma_h, \, \tau_h) + b(\tau_h, \, u_h) & =  & F(\tau_h), \, \fa \tau_h \in H_h \\
b(\sigma_h, \, \nu_h) & = & G(\nu_h), \, \fa \nu_h \in Q_h, 
%\end{aligned}
\end{array}
\right.
\label{discrete_mixed}
\end{equation}
\begin{theo}
Let $V_h := \mathrm{Ker}(\mathbf{B_h})$ and let $\Pi_h : H_h \rightarrow V_h$ be the orthogonal projection operator. Assume that:
\begin{enumerate}[label=(\roman*)]
\item $\Pi_h \mathbf{A_h} : V_h \rightarrow V_h$ is one-to-one
\item The bilinear form $b(\cdot , \, \cdot)$ satisfies the discrete LBB condition
\end{enumerate}
Then for each pair $(F, \, G) \in H' \times Q'$ there exists a unique $(\sigma_h, u_h) \in H_h \times Q_h$ solution of \ref{discrete_mixed}. Moreover there exists a constant $C_h > 0$, which depends on $\norm{\mathbf{A_h}}$, $\norm{(\Pi_h \mathbf{A_h})^{-1}}$, and $\beta_h$, such that:
\begin{equation}
\norm{(\sigma_h, \, u_h)}_{H_h \times Q_h} \leq C_h \paren{\norm{F}_{H'_h} + \norm{G}_{Q'_h}}
\end{equation}
\end{theo}
\begin{theo}[Cea's estimate]
If we have global and discrete stability, there holds:
\begin{equation}
\norm{(\sigma \, , \, u) - (\sigma_h \, , \, u_h)}_{H \times Q} \leq \norm{G_h} \inf_{(\zeta_h, u_h) \in H_h \times Q_h} \norm{(\sigma \, , \, u) - (\zeta_h \, ,\, w_h)}_{H \times Q}
\end{equation}
\end{theo}
Surely one can conclude that 
\[
\lim_{h \rightarrow 0}\norm{(\sigma \, , \, u) - (\sigma_h \, , \, u_h)}_{H \times Q} = 0
\]
if $\norm{G_h}$ is independent of h.
\begin{theo}[Fortin's lemma]
let H and Q be Hilbert spaces and $b : H \times Q \rightarrow \R$ a bounded bilinear form that satisfies the LBB condition on the global level (with inf-sup constant $\beta > 0$). Let $\setbra{H_h}_{h>0}$ and $\setbra{Q_h}_{h>0}$ be sequences of finite dimensional subspaces of H and Q respectively and assume the existence of a sequence of operators $\setbra{\Pi_h}_{h \in I} \subseteq \mathcal{L}(H, H_h)$ and $\widetilde{C} > 0$ such that:
\[
\norm{\Pi_h} \leq \widetilde{C}  \  \  \fa h \in I
\] 
and 
\[
b(\tau - \Pi_h(\tau) \, , \, v_h) = 0  \  \  \fa (\tau \, , \, v_h) \in H \times Q_h, \fa h \in I
\]
Then there exists $\widetilde{\beta} > 0$, independently of h, such that:
\[
\sup_{\tau_h \in H_h, \tau_h \neq 0} \frac{b(\tau_h \, , \, v_h)}{\norm{\tau_h}_{H}} \geq \widetilde{\beta} \norm{v_h}_{Q}
\]
with $\mathlarger{\widetilde{\beta} := \frac{\beta}{\widetilde{C}}}$
\end{theo}
%-----------------------------------
%	SUBSECTION 3.2
%-----------------------------------
\subsection{Spaces of polynomials}
\begin{dfn}
S bounded and convex domain:
\begin{equation}
\widetilde{\mathbb{P}}_k(S) := \setbra{p : S \rightarrow \R : p\operatorname{ is \ a  \ polynomial \ of \ degree} = k}
\end{equation}
and 
\begin{equation}
\mathbb{P}_k(S) := \setbra{p : S \rightarrow \R : p\operatorname{ is \ a  \ polynomial \ of \ degree} \leq k}
\end{equation}
\end{dfn}
We have the result $\operatorname{dim}\widetilde{\mathbb{P}}_k(S) = \left(\begin{array}{c c c c c} n & + & k & - & 1 \\ & & k & & \end{array}\right)$ from which one can show $\operatorname{dim}\mathbb{P}_k(S) = \left(\begin{array}{c c c} n & + & k \\ & k & \end{array}\right)$ 
\begin{dfn}[Raviart-Thomas spaces]
\[
RT_k(S) := [\mathbb{P}_k(S)]^{n} + \mathbb{P}_k(S)x \, ,
\]
\end{dfn}
%-----------------------------------
%	SUBSECTION 3.3
%-----------------------------------
\subsection{Local and Global Interpolation Operators}
\begin{theo}
Let $Z := \setbra{\tau \in \Ltwon \left| \tau_{|K} \in \left[ H^{1}(\Om)\right]^n \fa K \in \mathscr{T}_h \right.}$. Then
\[
\Hdiv \cap Z = \setbra{\tau \in Z\left| \tau \cdot \mathbf{n}_{K_i} +  \tau \cdot \mathbf{n}_{K_j} = 0 \fa K_i, K_j \in \mathscr{T}_h \operatorname{that \ are \ adjacent \ with \ common \ face/side \ F}\right. }
\]
\end{theo}
\begin{lem}
For each $K \in \mathscr{T}_h$ there holds:
\begin{enumerate}[label=(\roman*)]
\item $\operatorname{div}\tau \in \mathbb{P}_k(K) \ \fa \tau \in RT_k(K)$
\item $\left.  \tau \cdot \mathbf{n}_{K} \right|_F \in \mathbb{P}_k(F) \ \fa \operatorname{face/side \ F \ of \ K}, \ \fa \tau \in RT_k(K)$
\end{enumerate}
\end{lem}
\begin{dfn}[Global Raviart-Thomas spaces]
\[
H_h^k := \setbra{\tau \in \Hdiv \left| \tau_{|K} \in RT_k(K) \ \fa K \in \mathscr{T}_h \right.}
\]
\end{dfn}
\begin{dfn}[F-moments]
$\tau \in \Hdiv \cap Z $ and $\setbra{\psi_{1,F}, \dots , \psi_{d_k,F}}$ is a basis of $\mathbb{P}_k(F)$
\[
m_i(\tau) = \int_F \tau \cdot \mathbf{n}_F \psi_{l,F} \ \fa l \in \setbra{1, \dots, d_k}, \ \fa \operatorname{face/side \ F \ of} \mathscr{T}_h
\]
where $i \in \setbra{1, \dots , N_1}$ where $N_1$ is the number of faces/sides of $\mathscr{T}_h$ times $d_k$
\end{dfn}
\begin{dfn}[K-moments]
$\setbra{\psi_{1,K}, \dots , \psi_{r_k,K}}$ is a basis of $\mathbb{P}_{k-1}(K)$
\[
m_i(\tau) = \int_K \tau \cdot  \psi_{j,K} \ \fa j \in \setbra{1, \dots, r_k}, \ \fa K \in \mathscr{T}_h
\]
where $i \in \setbra{N_1+1, \dots , N}$ where $N - N_1$ is the number of polyhedra in $\mathscr{T}_h$ times $r_k$
\end{dfn}
\begin{dfn}[interpolating function]
Given $j \in \setbra{1, \dots , N}$, $\phi_j$ is the unique function in $H_h^k$ such that:
\[
m_i(\phi_j) =\delta_{ij} \ \fa i \in \setbra{1, \dots, N}
\] 
\end{dfn}
\begin{dfn}[Global interpolation operator]
$\Pi_{h}^{k} : H(\operatorname{div} ; \Omega) \cap Z \rightarrow H_{h}^{k}$
\[
\Pi_h^k(\tau) := \sum_{j=1}^{N}m_j(\tau)\phi_j
\]
\end{dfn}
\begin{dfn}[Local moments]
For each $K \in \mathscr{T}_h$ we let $m_{i,K}(\tau)$, $i \in \setbra{1, \dots, N_K}$, be the corresponding local moments, That is the F moments of local faces/sides F of K and the K-moments of K. With $N_K = (n+1)d_k+ r_k$
\end{dfn}
\begin{dfn}[Local interpolation operator]
$\Pi_{K}^{k} :\left[H^{1}(K)\right]^{n} \rightarrow R T_{k}(K)$
\[
\Pi_K^k(\tau) := \sum_{j=1}^{N_K}m_{j,K}(\tau)\phi_{j,K} \ \fa \tau \in  \left[ H^{1}(\Om)\right]^n
\]
\end{dfn}
Note that it holds $\Pi_{h}^{k}\left.(\tau)\right|_{K}=\Pi_{K}^{k}\left(\left.\tau\right|_{K}\right) \quad \forall \tau \in H(\operatorname{div} ; \Omega) \cap Z$.
\begin{dfn}[Orthogonal Projector]
%$\left.\right.$ \newline $\left.\right.$ \newline
$ \quad \mathscr{P}_{K}^{k} : L^{2}(K) \rightarrow \mathbb{P}_{k}(K)$ and $\mathscr{P}_{h}^{k} : L^{2}(\Omega) \rightarrow Y_{h}^{k}$ \\
\\
with $Y_{h}^{k} :=\left\{v \in L^{2}(\Omega) \left| \quad\left.v\right|_{K} \in \mathbb{P}_{k}(K) \quad \forall K \in \mathscr{T}_{h}\right.\right\}$
\end{dfn}
\begin{lem}
There holds:
\begin{equation}
\operatorname{div}\paren{\Pi_K^k(\tau)} = \mathscr{P}_K^k\paren{\operatorname{div}\tau} \quad \fa \tau \in \left[ H^{1}(K)\right]^n
\end{equation}
and 
\begin{equation}
\operatorname{div}\paren{\Pi_h^k(\tau)} = \mathscr{P}_h^k\paren{\operatorname{div}\tau} \quad \fa \tau \in H(\operatorname{div} ; \Omega) \cap Z
\end{equation}
\end{lem}
We have the following commuting diagram:
\begin{equation}
\begin{array}{r c l}
%\begin{aligned}
H(\operatorname{div} ; \Omega) \cap Z & \xrightarrow[]{\operatorname{div}} & \Ltwo \\
\Pi_h^k(\tau) \downarrow & \circlearrowright & \downarrow \mathscr{P}_h^k \\
	%\end{aligned}
\end{array}
\end{equation}
%-----------------------------------
%	SUBSECTION 3.4
%-----------------------------------
\subsection{Global interpolation error and local estimates}
\begin{lem}
Let S and $\widehat{S}$ be compact and connected sets of $\R^n$ with Lipschitz-continuous boundaries, and let $F : \R^n \rightarrow \R^n$ be the affine mapping given by $F(\widehat{x}) = B\widehat{x} + b$ where B is a $\R^{n \times n}$ invertible matrix and $b \in \R^n$, such that $S = F(\widehat{S})$. Next, let:
\begin{center}
$h_S :=$ diameter of S $= \underset{x,y \in S}{\operatorname{max}} \norm{x - y}$,\\
$\rho_S :=$ diameter of the largest sphere contained in S.\\
\end{center}
Then we have 
\begin{equation}
|\operatorname{det} B| = \frac{|S|}{\hat{|S|}}, \ \norm{B} \leq \frac{h_S}{\widehat{\rho}} \ and \ \norm{B^{-1}} \leq \frac{\widehat{h}}{\rho_S}
\end{equation}
\end{lem}
\begin{lem}[Local Interpolation Error]
Let m and k be nonnegative integers such that $0 \leq m \leq k + 1$. Then there exists $C := C(\widehat{K}, \Pi_K^k,k,m,n) > 0$ such that:
\begin{equation}
\left|\tau - \Pi_K^k(\tau)\right|_{m,K} \leq C \frac{h_K^{k+2}}{\rho_K^{m+1}}|\tau|_{k+1, K} \quad \fa \tau \in \left[H^{k+1}(K)\right]^n.
\end{equation}
\end{lem}
\begin{lem}[Local Interpolation Error]
Let m, k and l be nonnegative integers such that $0 \leq l \leq k $ and $0 \leq m \leq l + 1$. Then there exists $C := C(\widehat{K}, \Pi_K^k,k,m,n) > 0$ such that:
\begin{equation}
\left|\tau - \Pi_K^k(\tau)\right|_{m,K} \leq C \frac{h_K^{l+2}}{\rho_K^{m+1}}|\tau|_{k+1, K} \quad \fa \tau \in \left[H^{l+1}(K)\right]^n,
\end{equation}
and for each $\tau \in \left[H^{k+1}(K)\right]^n$ with $\operatorname{div}\tau \in H^{l+1}(K) $
\begin{equation}
\left|\operatorname{div}\tau - \operatorname{div}\Pi_K^k(\tau)\right|_{m,K} \leq C \frac{h_K^{l+1}}{\rho_K^{m}}|\operatorname{div}\tau|_{l+1, K}.
\end{equation}
\end{lem}
\begin{theo}[Global Interpolation Error]
Let $\setbra{\mathscr{T}_h}_{h>0}$ be a regular family of triangularizations of $\overline{\Om}$, and let k be a nonnegative integer. Then there exists $C>0$, independently of h, such that:
\begin{equation}
\norm{\tau - \Pi_h^k(\tau)}_{\operatorname{div}, \Om} \leq Ch^{l+1}\setbra{|\tau|_{l+1, \Om}+|\operatorname{div}\tau|_{l+1, \Om}}
\end{equation}
for each $\tau \in \left[H^{l+1}(\Om)\right]^n$, with $\operatorname{div}\tau \in H^{l+1}(\Om)$, $0 \leq l \leq k$
\end{theo}
\begin{lem}[Local interpolation error for normal components]
There exists $C > 0$, independently of h, such that $\fa K \in \mathscr{T}_h$, $\fa $face/side F of K, and $\fa \tau \in  \left[H^{1}(K)\right]^n $, there holds:
\begin{equation}
\norm{\tau \cdot \mathbf{n}_F - \Pi_K^k(\tau) \cdot \mathbf{n}_F }_{0,F} \leq C |F|^{\frac{1}{2}}|\tau|_{1,K}
\end{equation}
\end{lem}
\begin{lem}[Local Interpolation Error with Fractional Order]
Given $\delta \in ]0, \, 1[$ and $\tau \in \left[H^{\delta}(\Om)\right]^n \cap \Hdiv$, there holds:
\begin{equation}
\norm{\tau - \Pi_K^0(\tau)}_{0,K} \leq Ch_k^{\delta}\setbra{|\tau|_{\delta, K} + \norm{\operatorname{div}\tau}_{0, K}} \quad \fa K \in \mathscr{T}_h
\end{equation}
\end{lem}
%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Application to Small Strains Elasticity}
%-----------------------------------
%	SUBSECTION 3.1
%-----------------------------------
\subsection{Strong form}
$H := \mathrm{normed \ space}$\\
$\mathbf{H} := H^n$\\
$\mathbb{H} := H^{n \times n}$\\
$\mathbb{H}(\mathrm{div}; \Om) := \setbra{\left. \boldsymbol{\tau} \in \mathbb{L}^2(\Om) \right| \nabla \cdot \boldsymbol{\tau} \in \mathbf{L}^2(\Om)}$\\
The objective is to find $(\mathbf{u}, \Cstress) \in U \times V$ (where U and V are Hilbert spaces yet to be established)
\begin{equation}
\left\{
\begin{array}{r c r l}
\boldsymbol{\sigma} &=& \mathbb{C} : \boldsymbol{\varepsilon}(\mathbf{u}) & \mathrm{in} \ \Om \\
\nabla \cdot \boldsymbol{\sigma} &=&  \mathbf{f} & \mathrm{in} \ \Om \\
\mathbf{u} &=& 0 & \mathrm{on} \ \Ga_D \\
%\boldsymbol{\sigma} \mathbf{n} &=& \mathbf{g} & \mathrm{on} \ \Ga_N.
\end{array}
\right.
\label{Strong_form_elasticity_small}
\end{equation}
Where $\boldsymbol{\varepsilon}(\mathbf{u}) = \frac{1}{2}\paren{\nabla \mathbf{u} + \nabla^T \mathbf{u}} $ is the (linearized) deformation tensor. Now by posing $\boldsymbol{\rho} = \frac{1}{2}\paren{\nabla \mathbf{u} - \nabla^T \mathbf{u}}$ which is the anti-symmetric part of $\nabla \mathbf{u}$ we can rewrite the strong form \cref{Strong_form_elasticity_small} as:
\begin{equation}
\left\{
\begin{array}{r c r l}
\mathbb{C}^{-1} : \boldsymbol{\sigma} +  \nabla \mathbf{u} + \boldsymbol{\rho} &=& 0 & \mathrm{in} \ \Om \\
\nabla \cdot \boldsymbol{\sigma} &=&  \mathbf{f} & \mathrm{in} \ \Om \\
\mathbf{u} &=& 0 & \mathrm{on} \ \Ga_D \\
%\boldsymbol{\sigma} \mathbf{n} &=& \mathbf{g} & \mathrm{on} \ \Ga_N.
\end{array}
\right.
\label{Alternative_strong_small}
\end{equation}
In the problem \ref{Alternative_strong_small} we are looking for $(\mathbf{u}, \Cstress, \boldsymbol{\rho}) \in U \times V \times W$. By multiplying by test functions and integrate by parts when necessary, we obtain the weak form as follows:\\
Find $\paren{\mathbf{u}, \Cstress, \boldsymbol{\rho}} \in \mathbf{L}^2(\Om) \times \mathbb{H}(\mathrm{div}; \Om) \times \mathbf{AS}$
\begin{equation}
\left\{
\begin{array}{r c c l}
\paren{\mathbb{C}^{-1} : \boldsymbol{\sigma}, \mathbf{v}}_{\Om} +   \paren{\mathbf{u} , \mathrm{div}(\mathbf{v})}_{\Om}+ \paren{\boldsymbol{\rho}, \mathbf{v}}_{\Om}&=& 0, & \fa \mathbf{v} \in \mathbb{H}(\mathrm{div}; \Om) \\
\paren{\nabla \cdot \boldsymbol{\sigma}, \mathbf{w} }_{\Om}&=&  \paren{\mathbf{f}, \mathbf{w}}_{\Om}, & \fa \mathbf{w} \in \mathbf{L}^2(\Om)   \\
\paren{\Cstress, \boldsymbol{\eta}}_{\Om} &=& 0, & \fa \boldsymbol{\eta} \in \mathbf{AS}(\Om) \\
%\boldsymbol{\sigma} \mathbf{n} &=& \mathbf{g} & \mathrm{on} \ \Ga_N.
\end{array}
\right.
\label{eqn:weakformulation}
\end{equation}
where $\mathbf{AS}(\Om) := \setbra{\left. \boldsymbol{\eta} \in \mathbb{L}^2(\Om)\right| \boldsymbol{\eta} +  \boldsymbol{\eta}^T = 0}$ where the symmetry of the stress tensor is weakly enforced through the third equation in ~\eqref{eqn:weakformulation}. Given an admissible triangulation $\mathscr{T}_{h}$ of the domain $\Om$ we have the following discretized weak form:
\begin{equation}
\left\{
\begin{array}{r c c l}
\paren{\mathbb{C}^{-1} : \boldsymbol{\sigma}^h, \mathbf{v}}_{\Om} +   \paren{\mathbf{u}^h , \mathrm{div}(\mathbf{v})}_{\Om}+ \paren{\boldsymbol{\rho}^h, \mathbf{v}}_{\Om}&=& 0, & \fa \mathbf{v} \in \mathbf{V}^h \\
\paren{\nabla \cdot \boldsymbol{\sigma}^h, \mathbf{w} }_{\Om}&=&  \paren{\mathbf{f}, \mathbf{w}}_{\Om}, & \fa \mathbf{w} \in \mathbf{W}^h  \\
\paren{\Cstress^h, \boldsymbol{\eta}}_{\Om} &=& 0, & \fa \boldsymbol{\eta} \in \mathbf{A}^h\\
%\boldsymbol{\sigma} \mathbf{n} &=& \mathbf{g} & \mathrm{on} \ \Ga_N.
\end{array}
\right.
\label{discretized_weak}
\end{equation}
Where in \eqref{discretized_weak} we have:
\begin{equation}
\left\{
\begin{array}{r c l l}
\mathbf{V}^h & := & \setbra{\left. \mathbf{v} \in \mathbb{H}(\mathrm{div}; \Om) \right| \mathbf{v}_{|K} \in \mathbb{V}(K) }\\
\mathbf{W}^h & := & \setbra{\left. \mathbf{w} \in \mathbf{L}^2(\Om)  \right| \mathbf{w}_{|K} \in \mathbf{W}(K) }\\
\mathbf{A}^h & := & \setbra{\left. \boldsymbol{\eta} \in \mathbf{L}^2(\Om)  \right| \boldsymbol{\eta}_{|K} \in \mathbf{A}(K) }\\
\end{array}
\right.
\label{eqn:weakformulation}
\end{equation}
Where the local spaces $\mathbb{V}(K), \mathbf{W}(K), \mathbf{A}(K)$ must be chosen carefully for stability purposes. Following Cockburn's article \parencite{Cockburn2010} we choose the local spaces as:
\begin{equation}
\begin{array}{r l l l }
\mathbb{V}(K) & := & \mathbb{V}^k(K) + \delta\mathbb{V}^k(K) := \mathbb{R}\mathbb{T}^k(K) + \mathrm{curl}(\mathrm{curl}(\tilde{\mathbf{A}}^k)\underline{\mathbf{b}}_K)\\
\mathbf{W}(K) & := & \mathbf{W}^k(K) :=\mathbf{P}^k(K)\\
\mathbf{A}(K) & := & \mathbf{A}^k(K) :=  \setbra{\left. \boldsymbol{\eta} \in \mathbf{P}^k(K)  \right| \boldsymbol{\eta} + \boldsymbol{\eta}^T = 0}\\
\end{array}
\label{local_spaces}
\end{equation}
\parencite{Douglas2007}, \parencite{bubbleMat}
%-----------------------------------
%	SUBSECTION 3.2
%-----------------------------------

\subsection{Mixed Formulation}


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Discretisation and link with deRham cohomology}


%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------

\section{Results obtained with MoFEM}












