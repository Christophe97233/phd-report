\begin{thebibliography}{10}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Bathe(1996)]{Bathe1996}
K.~Bathe.
\newblock \emph{Finite {E}lement {P}rocedures}.
\newblock Prentice {H}all, Englewood Cliffs, New Jersey, 1996.

\bibitem[Douglas and Roberts(1985)]{Douglas1985}
J.~Douglas and J.~E. Roberts.
\newblock Global estimates for mixed methods for second order elliptic
  equations.
\newblock \emph{Math. {C}omp.}, 44\penalty0 (169):\penalty0 39 -- 52, 1985.

\bibitem[Falk and Osborn(1980)]{Falk1980}
R.~S. Falk and J.~E. Osborn.
\newblock Error estimates for mixed methods.
\newblock \emph{R{A}{I}{R}{O} {A}nalyse numerique}, 14\penalty0 (3):\penalty0
  249 -- 277, 1980.

\bibitem[Gil et~al.(2015)Gil, Bonet, and Ortigosa]{Gil}
A.~Gil, J.~Bonet, and R.~Ortigosa.
\newblock A computational framework for polyconvex large strain elasticity.
\newblock \emph{Computer methods in applied mechanics and engineering},
  283:\penalty0 1061--1094, 2015.

\bibitem[Lovadina and Stenberg(2006)]{Lovadina2006}
C.~Lovadina and R.~Stenberg.
\newblock Energy norm a posteriori error estimates for mixed finite element
  methods.
\newblock \emph{Math. {C}omp.}, 75:\penalty0 1659 -- 1674, 2006.

\bibitem[Morley(1989)]{Morley1989}
M.~E. Morley.
\newblock A family of mixed finite elements for linear elasticity.
\newblock \emph{Numerische Mathematik}, 55\penalty0 (6):\penalty0 633--666, Nov
  1989.

\bibitem[Nedelec(1980)]{Nedelec1980}
J.~C. Nedelec.
\newblock Mixed finite elements in {$\mathbb{R}^{3}$}.
\newblock \emph{Numerische {M}athematik}, 35\penalty0 (3):\penalty0 315--341,
  Sep 1980.

\bibitem[Stenberg(1988)]{Stenberg1988}
R.~Stenberg.
\newblock A family of mixed finite elements for the elasticity problem.
\newblock \emph{Numerische {M}athematik}, 53\penalty0 (5):\penalty0 513--538,
  Aug 1988.

\bibitem[Sussman and Bathe(1987)]{Sussman1987}
T.~Sussman and K.~Bathe.
\newblock A finite element formulation for nonlinear incompressible elastic and
  inelastic analysis.
\newblock \emph{Computers \& {S}tructures}, 26\penalty0 (1):\penalty0 357 --
  409, 1987.

\bibitem[Wriggers et~al.(2011)Wriggers, Schroder, and Balzani]{Wriggers}
P.~Wriggers, J.~Schroder, and D.~Balzani.
\newblock A new mixed finite element based on different approximations of the
  minors of deformation tensors.
\newblock \emph{Computer methods in applied mechanics and engineering},
  200:\penalty0 3583--3600, 2011.

\end{thebibliography}
