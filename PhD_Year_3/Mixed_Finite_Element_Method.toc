\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {chapter}{Declaration of Authorship}{iii}{section*.1}
\contentsline {chapter}{Abstract}{vii}{section*.2}
\contentsline {chapter}{Acknowledgements}{ix}{section*.3}
\contentsline {chapter}{\numberline {1}Elasticity in Large strains with classical formulation}{1}{chapter.14}
\contentsline {section}{\numberline {1.1}Laws of thermodynamics}{1}{section.15}
\contentsline {section}{\numberline {1.2}Laws of Thermodynamics}{1}{section.16}
\contentsline {subsection}{\numberline {1.2.1}Thermodynamics Principles}{1}{subsection.17}
\contentsline {subsubsection}{The first law of thermodynamics}{1}{section*.18}
\contentsline {subsubsection}{The second law of thermodynamics}{2}{section*.32}
\contentsline {subsection}{\numberline {1.2.2}Thermodynamics with internal variables}{3}{subsection.41}
\contentsline {subsection}{\numberline {1.2.3}Theory of hyperelasticity}{5}{subsection.52}
\contentsline {subsubsection}{Overview}{5}{section*.53}
\contentsline {subsubsection}{Different type of functionals}{8}{section*.82}
\contentsline {subsection}{\numberline {1.2.4}Possible applications}{8}{subsection.83}
\contentsline {chapter}{\numberline {2}Mixed Finite Element Formulation}{9}{chapter.84}
\contentsline {section}{\numberline {2.1}Motivation for Mixed Formulation}{9}{section.85}
\contentsline {section}{\numberline {2.2}Results from functional analysis}{9}{section.86}
\contentsline {section}{\numberline {2.3}The Mixed formulation for the Poisson problem}{9}{section.89}
\contentsline {section}{\numberline {2.4}Small strain elasticity}{9}{section.90}
\contentsline {subsection}{\numberline {2.4.1}Definitions from functional analysis}{9}{subsection.91}
\contentsline {subsection}{\numberline {2.4.2}Classical form of the mixed problem}{10}{subsection.102}
\contentsline {subsection}{\numberline {2.4.3}Stability results}{11}{subsection.109}
\contentsline {chapter}{\numberline {3}Discretisation for Mixed Formulation}{13}{chapter.125}
\contentsline {section}{\numberline {3.1}Galerkin scheme}{13}{section.126}
\contentsline {section}{\numberline {3.2}Spaces of polynomials}{14}{section.135}
\contentsline {section}{\numberline {3.3}Local and Global Interpolation Operators}{14}{section.140}
\contentsline {section}{\numberline {3.4}Global interpolation error and local estimates}{16}{section.157}
\contentsline {section}{\numberline {3.5}Application to Small Strains Elasticity}{17}{section.171}
\contentsline {subsection}{\numberline {3.5.1}Strong form}{17}{subsection.172}
\contentsline {subsection}{\numberline {3.5.2}Mixed Formulation}{18}{subsection.179}
\contentsline {section}{\numberline {3.6}Discretisation and link with deRham cohomology}{18}{section.180}
\contentsline {section}{\numberline {3.7}Schur Complement}{18}{section.181}
\contentsline {section}{\numberline {3.8}Results obtained with MoFEM}{18}{section.182}
\contentsline {chapter}{\numberline {4}Plasticity with mixed formulation in large strains}{19}{chapter.183}
\contentsline {section}{\numberline {4.1}Strong Form of the problem}{19}{section.184}
\contentsline {section}{\numberline {4.2}Weak Form}{19}{section.185}
\contentsline {section}{\numberline {4.3}Discretisation}{19}{section.186}
\contentsline {chapter}{\numberline {A}Small strains}{21}{appendix.187}
\contentsline {section}{\numberline {A.1}Elements of classical (small strains) plasticity}{21}{section.188}
\contentsline {subsection}{\numberline {A.1.1}Plasticity in small strains}{21}{subsection.189}
\contentsline {subsubsection}{The Von-Mises model with isotropic linear hardening}{22}{section*.204}
\contentsline {subsection}{\numberline {A.1.2}The return-mapping algorithm in small strains}{23}{subsection.210}
\contentsline {section}{\numberline {A.2}Numerical implementation for some problems}{24}{section.212}
\contentsline {subsection}{\numberline {A.2.1}1D problem : beam in traction}{24}{subsection.213}
\contentsline {subsection}{\numberline {A.2.2}3D problem : Plate with circular hole in traction}{24}{subsection.217}
\contentsline {chapter}{References}{33}{appendix*.223}
