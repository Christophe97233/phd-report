\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 1
\BOOKMARK [0][-]{chapter.2}{Functional spaces}{}% 2
\BOOKMARK [1][-]{section.2.1}{The Lp\(\) spaces}{chapter.2}% 3
\BOOKMARK [1][-]{section.2.2}{The Sobolev spaces Wm,p\(\)}{chapter.2}% 4
\BOOKMARK [1][-]{section.2.3}{The trace operator in Sobolev spaces}{chapter.2}% 5
\BOOKMARK [1][-]{section.2.4}{Porperties of Sobolev spaces}{chapter.2}% 6
\BOOKMARK [1][-]{section.2.5}{Sobolev spaces of negative order}{chapter.2}% 7
\BOOKMARK [1][-]{section.2.6}{The Hdiv space and normal traces}{chapter.2}% 8
\BOOKMARK [1][-]{section.2.7}{Conclusion}{chapter.2}% 9
\BOOKMARK [0][-]{chapter.3}{Thermodynamics \046 Continuum mechanics}{}% 10
\BOOKMARK [1][-]{section.3.1}{Kinematics}{chapter.3}% 11
\BOOKMARK [2][-]{subsection.3.1.1}{Configurations}{section.3.1}% 12
\BOOKMARK [2][-]{subsection.3.1.2}{Volume in the deformed configuration}{section.3.1}% 13
\BOOKMARK [2][-]{subsection.3.1.3}{Area in the deformed configuration}{section.3.1}% 14
\BOOKMARK [2][-]{subsection.3.1.4}{Length in the deformed configuration}{section.3.1}% 15
\BOOKMARK [2][-]{subsection.3.1.5}{The polar decomposition}{section.3.1}% 16
\BOOKMARK [2][-]{subsection.3.1.6}{The exponential map}{section.3.1}% 17
\BOOKMARK [2][-]{subsection.3.1.7}{Rotation and exponential map}{section.3.1}% 18
\BOOKMARK [2][-]{subsection.3.1.8}{Exponential of a symmetric tensor}{section.3.1}% 19
\BOOKMARK [1][-]{section.3.2}{The Cauchy stress and the Piola-Kirchhoff stress tensors}{chapter.3}% 20
\BOOKMARK [2][-]{subsection.3.2.1}{The stress principle}{section.3.2}% 21
\BOOKMARK [2][-]{subsection.3.2.2}{The Piola-Kirchhoff stress tensor}{section.3.2}% 22
\BOOKMARK [1][-]{section.3.3}{The first law of thermodynamics}{chapter.3}% 23
\BOOKMARK [1][-]{section.3.4}{The second law of thermodynamics}{chapter.3}% 24
\BOOKMARK [1][-]{section.3.5}{Conclusion}{chapter.3}% 25
\BOOKMARK [0][-]{chapter.4}{Elasticity in Large strains with classical formulation}{}% 26
\BOOKMARK [1][-]{section.4.1}{Constitutive equations and polyconvexity}{chapter.4}% 27
\BOOKMARK [2][-]{subsection.4.1.1}{Definition of an elastic material}{section.4.1}% 28
\BOOKMARK [2][-]{subsection.4.1.2}{ The material frame indifference}{section.4.1}% 29
\BOOKMARK [2][-]{subsection.4.1.3}{Hyperelasticity}{section.4.1}% 30
\BOOKMARK [2][-]{subsection.4.1.4}{Stored energy function and convexity}{section.4.1}% 31
\BOOKMARK [2][-]{subsection.4.1.5}{Polyconvexity}{section.4.1}% 32
\BOOKMARK [2][-]{subsection.4.1.6}{Linearisation of an hyperelastic Mooney-Rivlin model}{section.4.1}% 33
\BOOKMARK [1][-]{section.4.2}{Conclusion}{chapter.4}% 34
\BOOKMARK [0][-]{chapter.5}{PDEs in mixed form for elliptic problems}{}% 35
\BOOKMARK [1][-]{section.5.1}{Formulation and well-posedness in Hilbert spaces}{chapter.5}% 36
\BOOKMARK [1][-]{section.5.2}{A priori estimates}{chapter.5}% 37
\BOOKMARK [1][-]{section.5.3}{Saddle-point problems in Hilbert spaces}{chapter.5}% 38
\BOOKMARK [1][-]{section.5.4}{Babuska-Brezzi theorem}{chapter.5}% 39
\BOOKMARK [1][-]{section.5.5}{Conclusion}{chapter.5}% 40
\BOOKMARK [0][-]{chapter.6}{Polynomial spaces and Discretisation}{}% 41
\BOOKMARK [1][-]{section.6.1}{Notations}{chapter.6}% 42
\BOOKMARK [1][-]{section.6.2}{Legendre polynomials}{chapter.6}% 43
\BOOKMARK [1][-]{section.6.3}{Legendre polynomials: some properties}{chapter.6}% 44
\BOOKMARK [1][-]{section.6.4}{Jacobi polynomials}{chapter.6}% 45
\BOOKMARK [1][-]{section.6.5}{Homogenisation}{chapter.6}% 46
\BOOKMARK [1][-]{section.6.6}{Integrated Legendre polynomials}{chapter.6}% 47
\BOOKMARK [1][-]{section.6.7}{Integrated Jacobi polynomials}{chapter.6}% 48
\BOOKMARK [1][-]{section.6.8}{Discretisation}{chapter.6}% 49
\BOOKMARK [1][-]{section.6.9}{Description of the Finite Element}{chapter.6}% 50
\BOOKMARK [2][-]{subsection.6.9.1}{Polynomial space}{section.6.9}% 51
\BOOKMARK [2][-]{subsection.6.9.2}{Degrees of freedom}{section.6.9}% 52
\BOOKMARK [1][-]{section.6.10}{Conclusion}{chapter.6}% 53
\BOOKMARK [0][-]{chapter.7}{Mixed finite elements approximation}{}% 54
\BOOKMARK [1][-]{section.7.1}{Well-posedness and error analysis in conforming Galerkin approximation}{chapter.7}% 55
\BOOKMARK [1][-]{section.7.2}{Algebraic setting}{chapter.7}% 56
\BOOKMARK [1][-]{section.7.3}{Conclusion}{chapter.7}% 57
\BOOKMARK [0][-]{chapter.8}{Elasticity in Large strains with mixed formulation }{}% 58
\BOOKMARK [1][-]{section.8.1}{Mixed element in small strains with relaxed symmetry}{chapter.8}% 59
\BOOKMARK [1][-]{section.8.2}{Extension to large strains}{chapter.8}% 60
\BOOKMARK [2][-]{subsection.8.2.1}{Equations in weak form: Spatial problem}{section.8.2}% 61
\BOOKMARK [2][-]{subsection.8.2.2}{Discretisation}{section.8.2}% 62
\BOOKMARK [1][-]{section.8.3}{Schur optimisation}{chapter.8}% 63
\BOOKMARK [2][-]{subsection.8.3.1}{Spaces}{section.8.3}% 64
\BOOKMARK [1][-]{section.8.4}{Practical Construction of local polynomial base for Hdiv space}{chapter.8}% 65
\BOOKMARK [1][-]{section.8.5}{Degrees of freedom for Hdiv space }{chapter.8}% 66
\BOOKMARK [1][-]{section.8.6}{Comparison with results in classical formulation}{chapter.8}% 67
\BOOKMARK [2][-]{subsection.8.6.1}{Geometry of the Cook beam and boundary conditions}{section.8.6}% 68
\BOOKMARK [2][-]{subsection.8.6.2}{Cook benchmark problem}{section.8.6}% 69
\BOOKMARK [2][-]{subsection.8.6.3}{Results comparison}{section.8.6}% 70
\BOOKMARK [2][-]{subsection.8.6.4}{Convergence speed}{section.8.6}% 71
\BOOKMARK [2][-]{subsection.8.6.5}{Convergence}{section.8.6}% 72
\BOOKMARK [2][-]{subsection.8.6.6}{Compressible Column Bending}{section.8.6}% 73
\BOOKMARK [2][-]{subsection.8.6.7}{Torsion of a beam with a square cross section}{section.8.6}% 74
\BOOKMARK [1][-]{section.8.7}{Conclusion}{chapter.8}% 75
\BOOKMARK [0][-]{chapter.9}{Extension to plastic problems}{}% 76
\BOOKMARK [1][-]{section.9.1}{Application to small strains plasticity}{chapter.9}% 77
\BOOKMARK [2][-]{subsection.9.1.1}{Thermodynamics with internal variables}{section.9.1}% 78
\BOOKMARK [2][-]{subsection.9.1.2}{Plasticity in small strains}{section.9.1}% 79
\BOOKMARK [2][-]{subsection.9.1.3}{The Von-Mises model with isotropic linear hardening}{section.9.1}% 80
\BOOKMARK [1][-]{section.9.2}{Variational formulation of elasto-plastic problem}{chapter.9}% 81
\BOOKMARK [2][-]{subsection.9.2.1}{Functional spaces}{section.9.2}% 82
\BOOKMARK [2][-]{subsection.9.2.2}{Linear and bilinear forms}{section.9.2}% 83
\BOOKMARK [2][-]{subsection.9.2.3}{Discussion on the primal variational problem}{section.9.2}% 84
\BOOKMARK [1][-]{section.9.3}{Extension to plastic problems in large strains}{chapter.9}% 85
\BOOKMARK [1][-]{section.9.4}{Body}{chapter.9}% 86
\BOOKMARK [1][-]{section.9.5}{Kinematics with three configurations}{chapter.9}% 87
\BOOKMARK [1][-]{section.9.6}{Equilibrium in large strains}{chapter.9}% 88
\BOOKMARK [1][-]{section.9.7}{Constraints}{chapter.9}% 89
\BOOKMARK [1][-]{section.9.8}{Dissipation}{chapter.9}% 90
\BOOKMARK [1][-]{section.9.9}{Equations in strong form}{chapter.9}% 91
\BOOKMARK [0][-]{chapter.10}{Conclusion}{}% 92
\BOOKMARK [0][-]{chapter.11}{Acknowledgement}{}% 93
\BOOKMARK [0][-]{chapter.12}{Author's Declaration}{}% 94
\BOOKMARK [0][-]{section*.33}{Appendices}{}% 95
\BOOKMARK [0][-]{appendix.A}{The return-mapping algorithm in small strains}{}% 96
\BOOKMARK [1][-]{section.A.1}{Initial phase}{appendix.A}% 97
\BOOKMARK [1][-]{section.A.2}{Elastic hypothesis}{appendix.A}% 98
\BOOKMARK [1][-]{section.A.3}{Return-mapping algorithm}{appendix.A}% 99
\BOOKMARK [1][-]{section.A.4}{Local tangent modulus}{appendix.A}% 100
\BOOKMARK [1][-]{section.A.5}{Assembling and solving}{appendix.A}% 101
\BOOKMARK [1][-]{section.A.6}{Check global convergence}{appendix.A}% 102
\BOOKMARK [1][-]{section.A.7}{Update the plastic strains and post-process}{appendix.A}% 103
\BOOKMARK [1][-]{section.A.8}{Results for small strains}{appendix.A}% 104
\BOOKMARK [2][-]{subsection.A.8.1}{Internally pressurised cylinder}{section.A.8}% 105
\BOOKMARK [2][-]{subsection.A.8.2}{Stretching of a perforated rectangular plate}{section.A.8}% 106
\BOOKMARK [0][-]{appendix.B}{Results for large strains and classical formulation}{}% 107
\BOOKMARK [1][-]{section.B.1}{Possible applications}{appendix.B}% 108
\BOOKMARK [1][-]{section.B.2}{Stretching of a square perforated rubber sheet}{appendix.B}% 109
\BOOKMARK [0][-]{appendix.C}{Eigenvalues of a 3 by 3 matrix}{}% 110
\BOOKMARK [0][-]{appendix.D}{Algorithm for the eigenprojections of X}{}% 111
\BOOKMARK [0][-]{appendix.E}{Derivative of tensor logarithm}{}% 112
