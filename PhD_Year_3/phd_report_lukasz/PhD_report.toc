\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{8}{chapter.1}
\contentsline {chapter}{\numberline {2}Functional spaces}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}The $L^p(\Omega )$ spaces}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}The Sobolev spaces $W^{m,p}(\Omega )$}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}The trace operator in Sobolev spaces}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}Porperties of Sobolev spaces}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Sobolev spaces of negative order}{14}{section.2.5}
\contentsline {section}{\numberline {2.6}The $\mathbf {H}^{\mathrm {div}}$ space and normal traces}{15}{section.2.6}
\contentsline {section}{\numberline {2.7}Conclusion}{16}{section.2.7}
\contentsline {chapter}{\numberline {3}Thermodynamics \& Continuum mechanics}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Kinematics}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Configurations}{17}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Volume in the deformed configuration}{19}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Area in the deformed configuration}{19}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Length in the deformed configuration}{20}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}The polar decomposition}{21}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}The exponential map}{22}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Rotation and exponential map}{23}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Exponential of a symmetric tensor}{25}{subsection.3.1.8}
\contentsline {section}{\numberline {3.2}The Cauchy stress and the Piola-Kirchhoff stress tensors}{25}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}The stress principle}{25}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}The Piola-Kirchhoff stress tensor}{26}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}The first law of thermodynamics}{27}{section.3.3}
\contentsline {section}{\numberline {3.4}The second law of thermodynamics}{28}{section.3.4}
\contentsline {section}{\numberline {3.5}Conclusion}{29}{section.3.5}
\contentsline {chapter}{\numberline {4}Elasticity in Large strains with classical formulation}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}Constitutive equations and polyconvexity}{30}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Definition of an elastic material}{31}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2} The material frame indifference}{31}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Hyperelasticity}{31}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Stored energy function and convexity}{33}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Polyconvexity}{36}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Linearisation of an hyperelastic Mooney-Rivlin model}{37}{subsection.4.1.6}
\contentsline {section}{\numberline {4.2}Conclusion}{39}{section.4.2}
\contentsline {chapter}{\numberline {5}PDEs in mixed form for elliptic problems}{40}{chapter.5}
\contentsline {section}{\numberline {5.1}Formulation and well-posedness in Hilbert spaces}{40}{section.5.1}
\contentsline {section}{\numberline {5.2}A priori estimates}{41}{section.5.2}
\contentsline {section}{\numberline {5.3}Saddle-point problems in Hilbert spaces}{42}{section.5.3}
\contentsline {section}{\numberline {5.4}Babuska-Brezzi theorem}{42}{section.5.4}
\contentsline {section}{\numberline {5.5}Conclusion}{43}{section.5.5}
\contentsline {chapter}{\numberline {6}Polynomial spaces and Discretisation}{44}{chapter.6}
\contentsline {section}{\numberline {6.1}Notations}{44}{section.6.1}
\contentsline {section}{\numberline {6.2}Legendre polynomials}{45}{section.6.2}
\contentsline {section}{\numberline {6.3}Legendre polynomials: some properties}{45}{section.6.3}
\contentsline {section}{\numberline {6.4}Jacobi polynomials}{46}{section.6.4}
\contentsline {section}{\numberline {6.5}Homogenisation}{47}{section.6.5}
\contentsline {section}{\numberline {6.6}Integrated Legendre polynomials}{48}{section.6.6}
\contentsline {section}{\numberline {6.7}Integrated Jacobi polynomials}{48}{section.6.7}
\contentsline {section}{\numberline {6.8}Discretisation}{49}{section.6.8}
\contentsline {section}{\numberline {6.9}Description of the Finite Element}{50}{section.6.9}
\contentsline {subsection}{\numberline {6.9.1}Polynomial space}{51}{subsection.6.9.1}
\contentsline {subsection}{\numberline {6.9.2}Degrees of freedom}{52}{subsection.6.9.2}
\contentsline {section}{\numberline {6.10}Conclusion}{53}{section.6.10}
\contentsline {chapter}{\numberline {7}Mixed finite elements approximation}{54}{chapter.7}
\contentsline {section}{\numberline {7.1}Well-posedness and error analysis in conforming Galerkin approximation}{54}{section.7.1}
\contentsline {section}{\numberline {7.2}Algebraic setting}{56}{section.7.2}
\contentsline {section}{\numberline {7.3}Conclusion}{56}{section.7.3}
\contentsline {chapter}{\numberline {8}Elasticity in Large strains with mixed formulation }{57}{chapter.8}
\contentsline {section}{\numberline {8.1}Mixed element in small strains with relaxed symmetry}{57}{section.8.1}
\contentsline {section}{\numberline {8.2}Extension to large strains}{59}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Equations in weak form: Spatial problem}{60}{subsection.8.2.1}
\contentsline {paragraph}{Kinematics}{60}{paragraph*.6}
\contentsline {paragraph}{Consistency equation}{60}{paragraph*.7}
\contentsline {paragraph}{Physical equation}{61}{paragraph*.8}
\contentsline {paragraph}{Angular momentum equation}{61}{paragraph*.9}
\contentsline {paragraph}{Linear momentum equation}{61}{paragraph*.10}
\contentsline {paragraph}{Element equations}{61}{paragraph*.11}
\contentsline {subsection}{\numberline {8.2.2}Discretisation}{62}{subsection.8.2.2}
\contentsline {section}{\numberline {8.3}Schur optimisation}{63}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Spaces}{67}{subsection.8.3.1}
\contentsline {paragraph}{Approximation of logarithmic stretch}{67}{paragraph*.13}
\contentsline {section}{\numberline {8.4}Practical Construction of local polynomial base for $\mathbf {H}^{\mathrm {div}}$ space}{68}{section.8.4}
\contentsline {section}{\numberline {8.5}Degrees of freedom for $\mathbf {H}^{\mathrm {div}}$ space }{71}{section.8.5}
\contentsline {section}{\numberline {8.6}Comparison with results in classical formulation}{71}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Geometry of the Cook beam and boundary conditions}{73}{subsection.8.6.1}
\contentsline {subsection}{\numberline {8.6.2}Cook benchmark problem}{73}{subsection.8.6.2}
\contentsline {subsection}{\numberline {8.6.3}Results comparison}{74}{subsection.8.6.3}
\contentsline {subsection}{\numberline {8.6.4}Convergence speed}{79}{subsection.8.6.4}
\contentsline {subsection}{\numberline {8.6.5}Convergence}{80}{subsection.8.6.5}
\contentsline {subsection}{\numberline {8.6.6}Compressible Column Bending}{83}{subsection.8.6.6}
\contentsline {subsection}{\numberline {8.6.7}Torsion of a beam with a square cross section}{85}{subsection.8.6.7}
\contentsline {section}{\numberline {8.7}Conclusion}{89}{section.8.7}
\contentsline {chapter}{\numberline {9}Extension to plastic problems}{90}{chapter.9}
\contentsline {section}{\numberline {9.1}Application to small strains plasticity}{90}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}Thermodynamics with internal variables}{90}{subsection.9.1.1}
\contentsline {subsection}{\numberline {9.1.2}Plasticity in small strains}{92}{subsection.9.1.2}
\contentsline {subsection}{\numberline {9.1.3}The Von-Mises model with isotropic linear hardening}{95}{subsection.9.1.3}
\contentsline {section}{\numberline {9.2}Variational formulation of elasto-plastic problem}{96}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Functional spaces}{96}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Linear and bilinear forms}{96}{subsection.9.2.2}
\contentsline {subsection}{\numberline {9.2.3}Discussion on the primal variational problem}{97}{subsection.9.2.3}
\contentsline {section}{\numberline {9.3}Extension to plastic problems in large strains}{97}{section.9.3}
\contentsline {section}{\numberline {9.4}Body}{98}{section.9.4}
\contentsline {section}{\numberline {9.5}Kinematics with three configurations}{99}{section.9.5}
\contentsline {section}{\numberline {9.6}Equilibrium in large strains}{100}{section.9.6}
\contentsline {section}{\numberline {9.7}Constraints}{102}{section.9.7}
\contentsline {section}{\numberline {9.8}Dissipation}{103}{section.9.8}
\contentsline {section}{\numberline {9.9}Equations in strong form}{104}{section.9.9}
\contentsline {chapter}{\numberline {10}Conclusion}{105}{chapter.10}
\contentsline {chapter}{\numberline {11}Acknowledgement}{107}{chapter.11}
\contentsline {chapter}{\numberline {12}Author's Declaration}{108}{chapter.12}
\contentsline {chapter}{Appendices}{110}{section*.33}
\contentsline {chapter}{\numberline {A}The return-mapping algorithm in small strains}{110}{appendix.A}
\contentsline {section}{\numberline {A.1}Initial phase}{112}{section.A.1}
\contentsline {section}{\numberline {A.2}Elastic hypothesis}{112}{section.A.2}
\contentsline {section}{\numberline {A.3}Return-mapping algorithm}{113}{section.A.3}
\contentsline {section}{\numberline {A.4}Local tangent modulus}{115}{section.A.4}
\contentsline {section}{\numberline {A.5}Assembling and solving}{116}{section.A.5}
\contentsline {section}{\numberline {A.6}Check global convergence}{116}{section.A.6}
\contentsline {section}{\numberline {A.7}Update the plastic strains and post-process}{117}{section.A.7}
\contentsline {section}{\numberline {A.8}Results for small strains}{117}{section.A.8}
\contentsline {subsection}{\numberline {A.8.1}Internally pressurised cylinder}{118}{subsection.A.8.1}
\contentsline {subsection}{\numberline {A.8.2}Stretching of a perforated rectangular plate}{121}{subsection.A.8.2}
\contentsline {chapter}{\numberline {B}Results for large strains and classical formulation}{126}{appendix.B}
\contentsline {section}{\numberline {B.1}Possible applications}{126}{section.B.1}
\contentsline {section}{\numberline {B.2}Stretching of a square perforated rubber sheet}{126}{section.B.2}
\contentsline {chapter}{\numberline {C}Eigenvalues of a 3 by 3 matrix}{132}{appendix.C}
\contentsline {chapter}{\numberline {D}Algorithm for the eigenprojections of $\mathbf {X}$}{134}{appendix.D}
\contentsline {chapter}{\numberline {E}Derivative of tensor logarithm}{135}{appendix.E}
