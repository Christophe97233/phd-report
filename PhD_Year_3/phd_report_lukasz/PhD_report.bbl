\begin{thebibliography}{101}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Abhyankar et~al.(2018)Abhyankar, Brown, Constantinescu, Ghosh, Smith,
  and Zhang]{PetscTS}
S.~Abhyankar, J.~Brown, E.~M. Constantinescu, D.~Ghosh, B.~F. Smith, and
  H.~Zhang.
\newblock {PETSc/TS}: {A} modern scalable {ODE/DAE} solver library.
\newblock Technical report, 2018.

\bibitem[Adams and Fournier(2003)]{AdFour}
R.~Adams and J.~Fournier.
\newblock \emph{Sobolev spaces}, pages 61--67.
\newblock Pure and applied mathematics (Elsevier/Academic press), 2003.

\bibitem[Ainsworth and Coyle(2003)]{AinsOrient}
M.~Ainsworth and J.~Coyle.
\newblock Hierarchic finite element bases on unstructured tetrahedral meshes.
\newblock \emph{International Journal for Numerical Methods in Engineering},
  58\penalty0 (14):\penalty0 2103--2130, 2003.

\bibitem[Allaire(2012)]{Allaire}
G.~Allaire.
\newblock \emph{Analyse numerique et optimisation [Numerical analysis and
  optimisation]}, pages 81--107.
\newblock Editions de l'Ecole Polytechnique, 2 edition, 2012.

\bibitem[Amara and Thomas(1979)]{amara1979}
M.~Amara and J.-M. Thomas.
\newblock Equilibrium finite elements for the linear elastic problem.
\newblock \emph{Numerische Mathematik}, 33\penalty0 (4):\penalty0 367--383,
  1979.

\bibitem[Argyris(1954)]{Argyris}
J.~Argyris.
\newblock Energy theorems and structural analysis, part l.
\newblock \emph{Aircraft Eng.}, 26:\penalty0 383, 1954.

\bibitem[Arnold and Brezzi(1985)]{ArtBrez}
D.~N. Arnold and F.~Brezzi.
\newblock Mixed and nonconforming finite element methods : implementation,
  postprocessing and error estimates.
\newblock \emph{ESAIM: M2AN}, 19\penalty0 (1):\penalty0 7--32, 1985.

\bibitem[Atluri and Cazzani(1995)]{atluri1995}
S.~Atluri and A.~Cazzani.
\newblock Rotations in computational solid mechanics.
\newblock \emph{Archives of Computational Methods in Engineering}, 2\penalty0
  (1):\penalty0 49--138, 1995.

\bibitem[Babushka(1970/71)]{BABUSKA}
I.~Babushka.
\newblock Error-bounds for finite element method.
\newblock \emph{Numerische Mathematik}, 16:\penalty0 322--333, 1970/71.

\bibitem[Balay et~al.(2022)Balay, Abhyankar, Adams, Benson, Brown, Brune,
  Buschelman, Constantinescu, Dalcin, Dener, Eijkhout, Faibussowitsch, Gropp,
  Hapla, Isaac, Jolivet, Karpeev, Kaushik, Knepley, Kong, Kruger, May, McInnes,
  Mills, Mitchell, Munson, Roman, Rupp, Sanan, Sarich, Smith, Zampini, Zhang,
  Zhang, and Zhang]{petsc-web-page}
S.~Balay, S.~Abhyankar, M.~F. Adams, S.~Benson, J.~Brown, P.~Brune,
  K.~Buschelman, E.~M. Constantinescu, L.~Dalcin, A.~Dener, V.~Eijkhout,
  J.~Faibussowitsch, W.~D. Gropp, V.~Hapla, T.~Isaac, P.~Jolivet, D.~Karpeev,
  D.~Kaushik, M.~G. Knepley, F.~Kong, S.~Kruger, D.~A. May, L.~C. McInnes,
  R.~T. Mills, L.~Mitchell, T.~Munson, J.~E. Roman, K.~Rupp, P.~Sanan,
  J.~Sarich, B.~F. Smith, S.~Zampini, H.~Zhang, H.~Zhang, and J.~Zhang.
\newblock {PETS}c {W}eb page.
\newblock \url{https://petsc.org/}, 2022.
\newblock URL \url{https://petsc.org/}.

\bibitem[Ball(1976)]{JBall}
J.~M. Ball.
\newblock Convexity conditions and existence theorems in nonlinear elasticity.
\newblock \emph{Archive for Rational Mechanics and Analysis}, 63\penalty0
  (4):\penalty0 337--403, 1976.

\bibitem[Bathe(1996)]{Bathe1996}
K.~Bathe.
\newblock \emph{Finite {E}lement {P}rocedures}.
\newblock Prentice {H}all, Englewood Cliffs, New Jersey, 1996.

\bibitem[Brenner(1999)]{SchurStable}
S.~Brenner.
\newblock The condition number of the schur complement in domain decomposition.
\newblock \emph{Numer. Math.}, 83:\penalty0 187 -- 203, 1999.

\bibitem[Brezis(2011)]{Brezisbook}
H.~Brezis.
\newblock \emph{Functional Analysis, Sobolev Spaces and Partial Differential
  Equations}.
\newblock Springer, N-Y, 2011.

\bibitem[Brezzi et~al.(1984)Brezzi, Douglas, and Marini]{brezzi1984}
F.~Brezzi, J.~Douglas, and L.~Marini.
\newblock \emph{Recent results on mixed finite element methods for second order
  elliptic problems}.
\newblock Ist., Consiglio, 1984.

\bibitem[Carrera et~al.(2017)Carrera, {de Miguel}, and Pagani]{Carrera}
E.~Carrera, A.~{de Miguel}, and A.~Pagani.
\newblock Hierarchical theories of structures based on legendre polynomial
  expansions with finite element applications.
\newblock \emph{International Journal of Mechanical Sciences}, 120:\penalty0
  286--300, 2017.

\bibitem[Cheng and Gupta(1989)]{HistRodrigues}
H.~Cheng and K.~Gupta.
\newblock A historical note on finite rotations.
\newblock \emph{ASME J. Appl. Mech.}, 56:\penalty0 139--145, 1989.

\bibitem[Ciarlet(1988)]{CiarletTheo}
P.~Ciarlet.
\newblock \emph{Mathematical Elasticity}, volume~1, pages 170--172.
\newblock Elsevier Science Publisher, 1988.

\bibitem[Clough(1960)]{Clough2}
R.~Clough.
\newblock The finite element method, in plane stress analysis.
\newblock \emph{Proc. ASCE Structures Congress Session on Computer Utilization
  in Structural Eng.}, pages 1--10, 1960.

\bibitem[Clough(1990)]{Clough1}
R.~Clough.
\newblock Original formulation of the finite element method.
\newblock \emph{Finite Elements in Analysis and Design}, 7\penalty0
  (2):\penalty0 89--101, 1990.

\bibitem[Clough and Wilson(1962)]{Clough3}
R.~Clough and E.~Wilson.
\newblock Stress analysis of a gravity dam by the finite element method.
\newblock \emph{Proc. Symp. on the use of Computers in Civil Eng. Lab.}, 1962.

\bibitem[Cockburn et~al.(2010)Cockburn, Gopalakrishnan, and
  Guzman]{CockburnArt}
B.~Cockburn, J.~Gopalakrishnan, and J.~Guzman.
\newblock A new elasticity element made for enforcing weak stress symmetry.
\newblock \emph{Math. Comp.}, 79:\penalty0 1331--1349, 2010.

\bibitem[Courant(1942)]{Courant}
R.~Courant.
\newblock Variational methods for the solution of problems of equilibrium and
  vibrations.
\newblock \emph{Trans. Amer. Math. Soc.}, pages 1--23, 1942.

\bibitem[Dattoli et~al.(2001)Dattoli, Ricci, and
  Cesarano]{DattoliRicciCesarano}
G.~Dattoli, P.~Ricci, and C.~Cesarano.
\newblock A note on legendre polynomials.
\newblock \emph{International Journal of Nonlinear Sciences and Numerical
  Simulation}, 2\penalty0 (4):\penalty0 365--370, 2001.

\bibitem[de~Souza et~al.(2008{\natexlab{a}})de~Souza, Peric, and
  Owen]{DeSouzaDerLogarithm}
E.~de~Souza, D.~Peric, and D.~Owen.
\newblock \emph{Computational Methods for Plasticity, Theory and applications},
  pages 744--744.
\newblock Wiley, 2008{\natexlab{a}}.

\bibitem[de~Souza et~al.(2008{\natexlab{b}})de~Souza, Peric, and
  Owen]{DeSouzaFunctions}
E.~de~Souza, D.~Peric, and D.~Owen.
\newblock \emph{Computational Methods for Plasticity, Theory and applications},
  pages 742--752.
\newblock Wiley, 2008{\natexlab{b}}.

\bibitem[de~Souza et~al.(2008{\natexlab{c}})de~Souza, Peric, and
  Owen]{DeSouzaPipe}
E.~de~Souza, D.~Peric, and D.~Owen.
\newblock \emph{Computational Methods for Plasticity, Theory and applications},
  pages 244--247.
\newblock Wiley, 2008{\natexlab{c}}.

\bibitem[de~Souza et~al.(2008{\natexlab{d}})de~Souza, Peric, and
  Owen]{DeSouzaPlastAlgo}
E.~de~Souza, D.~Peric, and D.~Owen.
\newblock \emph{Computational Methods for Plasticity, Theory and applications},
  pages 191--265.
\newblock Wiley, 2008{\natexlab{d}}.

\bibitem[de~Souza et~al.(2008{\natexlab{e}})de~Souza, Peric, and
  Owen]{DeSouzaPlastTheo}
E.~de~Souza, D.~Peric, and D.~Owen.
\newblock \emph{Computational Methods for Plasticity, Theory and applications},
  pages 137--191.
\newblock Wiley, 2008{\natexlab{e}}.

\bibitem[de~Souza et~al.(2008{\natexlab{f}})de~Souza, Peric, and
  Owen]{DeSouzaStretchSS}
E.~de~Souza, D.~Peric, and D.~Owen.
\newblock \emph{Computational Methods for Plasticity, Theory and applications},
  pages 391--393.
\newblock Wiley, 2008{\natexlab{f}}.

\bibitem[de~Veubeke(1965)]{Fraejis}
F.~de~Veubeke.
\newblock Displacement and equilibrium models in the finite element method.
\newblock In \emph{Stress Analysis}. John Wiley \& Sons, 1965.

\bibitem[Delsarte et~al.(1991)Delsarte, Goethals, and Seidel]{Delsarte}
P.~Delsarte, J.~Goethals, and J.~Seidel.
\newblock Bounds for systems of lines and jacobi polynomials.
\newblock In D.~Corneil and R.~Mathon, editors, \emph{Geometry and
  Combinatorics}, pages 193--207. Academic Press, 1991.

\bibitem[Di~Nezza et~al.(2012)Di~Nezza, Palatucci, and Valdinoci]{Hitch}
E.~Di~Nezza, G.~Palatucci, and E.~Valdinoci.
\newblock Hitchhiker?s guide to the fractional sobolev spaces.
\newblock \emph{Bulletin des sciences math{\'e}matiques}, 136\penalty0
  (5):\penalty0 521--573, 2012.

\bibitem[Douglas and Roberts(1985)]{Douglas1985}
J.~Douglas and J.~E. Roberts.
\newblock Global estimates for mixed methods for second order elliptic
  equations.
\newblock \emph{Math. {C}omp.}, 44\penalty0 (169):\penalty0 39 -- 52, 1985.

\bibitem[Duvaut and Lions(1976)]{duvautinequalities}
G.~Duvaut and J.~Lions.
\newblock Inequalities in mechanics and physics.
\newblock 1976.

\bibitem[Ern and Guermond(2016)]{ErnLip}
A.~Ern and J.-L. Guermond.
\newblock Mollification in strongly lipschitz domains with application to
  continuous and discrete de rham complexes.
\newblock \emph{Comput. Methods Appl. Math.}, \penalty0 (16):\penalty0 51--75,
  2016.

\bibitem[Ern and Guermond(2021{\natexlab{a}})]{ErnG}
A.~Ern and J.-L. Guermond.
\newblock \emph{Finite Elements: Galerkin Approximation, Elliptic and Mixed
  PDEs}, volume~2, pages 111--112.
\newblock Springer, 2021{\natexlab{a}}.

\bibitem[Ern and Guermond(2021{\natexlab{b}})]{ErnSobolev}
A.~Ern and J.-L. Guermond.
\newblock \emph{Finite Elements: Approximation and Interpolation}, volume~1,
  pages 27--39.
\newblock Springer, 2021{\natexlab{b}}.

\bibitem[Evans(2003)]{Evans}
L.~Evans.
\newblock \emph{Partial Differential Equations}, volume~19, pages 61--67.
\newblock Graduate Studies in Mathematics, 2003.

\bibitem[Falk and Osborn(1980)]{Falk1980}
R.~S. Falk and J.~E. Osborn.
\newblock Error estimates for mixed methods.
\newblock \emph{R{A}{I}{R}{O} {A}nalyse numerique}, 14\penalty0 (3):\penalty0
  249 -- 277, 1980.

\bibitem[Farhloul and Fortin(1997)]{farhloul}
M.~Farhloul and M.~Fortin.
\newblock Dual hybrid methods for the elasticity and the stokes problems: a
  unified approach.
\newblock \emph{Numerische Mathematik}, 76\penalty0 (4):\penalty0 419--440,
  1997.

\bibitem[Felippa and Haugen(2005)]{Felippa}
C.~Felippa and B.~Haugen.
\newblock A unified formulation of small-strain corotational finite elements:
  I. theory.
\newblock \emph{Computer Methods in Applied Mechanics and Engineering},
  194\penalty0 (21):\penalty0 2285--2335, 2005.
\newblock Computational Methods for Shells.

\bibitem[Fortney(2018)]{fortney}
J.~Fortney.
\newblock \emph{A visual introduction to differential forms and calculus on
  manifolds}.
\newblock Springer, 2018.

\bibitem[Fuentes et~al.(2015)Fuentes, Keith, Demkowicz, and
  Nagaraj]{DemkowiczShape}
F.~Fuentes, B.~Keith, L.~Demkowicz, and S.~Nagaraj.
\newblock Orientation embedded high order shape functions for the exact
  sequence elements of all shapes.
\newblock \emph{Computers and Mathematics with Applications}, 70\penalty0
  (4):\penalty0 353--458, 2015.

\bibitem[Gagliardo(1957)]{gagliardo}
E.~Gagliardo.
\newblock Caratterizzazioni delle tracce sulla frontiera relative ad alcune
  classi di funzioni in $ n $ variabili.
\newblock \emph{Rendiconti del seminario matematico della universita di
  Padova}, 27:\penalty0 284--305, 1957.

\bibitem[Gamelin and Greene(1999)]{TheoTop}
T.~Gamelin and R.~E. Greene.
\newblock \emph{Introduction to Topology}.
\newblock Dover Publications, 2 edition, 1999.

\bibitem[Gil et~al.(2015)Gil, Bonet, and Ortigosa]{Gil}
A.~Gil, J.~Bonet, and R.~Ortigosa.
\newblock A computational framework for polyconvex large strain elasticity.
\newblock \emph{Computer methods in applied mechanics and engineering},
  283:\penalty0 1061--1094, 2015.

\bibitem[Gopalakrishnan and Guzman(2012)]{Gopala}
J.~Gopalakrishnan and J.~Guzman.
\newblock A second elasticity element using the matrix bubble.
\newblock \emph{IMA Journal of Numerical Analysis}, 32\penalty0 (1):\penalty0
  352--372, 2012.

\bibitem[Grisvard(1992)]{Grisvard}
P.~Grisvard.
\newblock Singularities in boundary value problem.
\newblock \emph{Recherches en Mathematiques Appliquees [Research in Applied
  Mathematics]}, 22:\penalty0 110 -- 115, 1992.

\bibitem[Guzman(2010)]{GuzmanArt}
J.~Guzman.
\newblock Unified analysis of several mixed methods for elasticity with weak
  stress symmetry.
\newblock \emph{J Sci Comput}, 44:\penalty0 156--169, 2010.

\bibitem[Hackbusch et~al.(2005)Hackbusch, Khoromskij, and
  Kriemann]{SchurDomain}
W.~Hackbusch, B.~Khoromskij, and R.~Kriemann.
\newblock Direct schur complement method by domain decomposition based on
  h-matrix approximation.
\newblock \emph{Comput. Visual Sci.}, 8:\penalty0 179 -- 188, 2005.

\bibitem[Han and Reddy(2012)]{Reddy}
W.~Han and B.~Reddy.
\newblock \emph{Plasticity: Mathematical Theory and Numerical Analysis}.
\newblock Springer, 2 edition, 2012.

\bibitem[Hill(1950)]{HillPipe}
R.~Hill.
\newblock \emph{The mathematical theory of plasticity}.
\newblock London: Oxford University Press, 1950.

\bibitem[Holzapfel(2000)]{holzap}
G.~Holzapfel.
\newblock \emph{Nonlinear Solid Mechanics: A continuum Approach for
  Engineering}.
\newblock Wiley, 2000.

\bibitem[Hughes et~al.(1977)Hughes, Taylor, and Kanoknukulchai]{hughes1977}
T.~Hughes, R.~Taylor, and W.~Kanoknukulchai.
\newblock A simple and efficient finite element for plate bending.
\newblock \emph{International Journal for Numerical Methods in Engineering},
  11\penalty0 (10):\penalty0 1529--1543, 1977.

\bibitem[Im et~al.(2020)Im, Cho, Kee, and Shin]{ChoIm}
B.~Im, H.~Cho, Y.~Kee, and S.~Shin.
\newblock Geometrically exact beam analysis based on the exponential map finite
  rotations.
\newblock \emph{Int. J. Aeronaut. Space Sci.}, 21:\penalty0 153--162, 2020.

\bibitem[Jirasek and Bazant(2001)]{jirasek2001}
M.~Jirasek and Z.~Bazant.
\newblock \emph{Inelastic analysis of structures}.
\newblock John Wiley \& Sons, 2001.

\bibitem[Kaczmarczyk et~al.(2020)Kaczmarczyk, Ullah, Lewandowski, Meng, Zhou,
  Athanasiadis, Nguyen, Chalons-Mouriesse, Richardson, Miur, Shvarts, Wakeni,
  and Pearce]{mofemJoss2020}
{\L}.~Kaczmarczyk, Z.~Ullah, K.~Lewandowski, X.~Meng, X.-Y. Zhou,
  I.~Athanasiadis, H.~Nguyen, C.-A. Chalons-Mouriesse, E.~Richardson, E.~Miur,
  A.~Shvarts, M.~Wakeni, and C.~Pearce.
\newblock {MoFEM:} an open source, parallel finite element library.
\newblock \emph{The Journal of Open Source Software}, 2020.
\newblock \doi{10.21105/joss.01441}.
\newblock URL \url{https://joss.theoj.org/papers/10.21105/joss.01441}.
\newblock http://mofem.eng.gla.ac.uk.

\bibitem[Kantorovich and Akilov(2016)]{kantorovich}
L.~Kantorovich and G.~Akilov.
\newblock \emph{Functional analysis}.
\newblock Elsevier, 2016.

\bibitem[Katz(1982)]{Katz}
V.~Katz.
\newblock Change of variables in multiple integrals: Euler to cartan.
\newblock \emph{Mathematics Magazine}, 55\penalty0 (1):\penalty0 3--11, 1982.

\bibitem[Kondepudi(2008)]{ThermoIntro}
D.~Kondepudi.
\newblock \emph{Introduction to modern thermodynamics}.
\newblock Wiley, 1 edition, 2008.

\bibitem[Lee(2013)]{LeeTop}
J.~Lee.
\newblock \emph{Introduction to Topological Manifolds}.
\newblock Springer New York, 2 edition, 2013.

\bibitem[Lewandowski et~al.(2023)Lewandowski, Barbera, Blackwell, Roohi,
  Athanasiadis, McBride, Steinmann, Pearce, and Kaczmarczyk]{KarolMultifield}
K.~Lewandowski, D.~Barbera, P.~Blackwell, A.~Roohi, I.~Athanasiadis,
  A.~McBride, P.~Steinmann, C.~Pearce, and L.~Kaczmarczyk.
\newblock A multifield formulation of finite strain plasticity.
\newblock \emph{SSRN}, 2023.

\bibitem[Lieb and Yngvason(1999)]{SecondLaw}
E.~H. Lieb and J.~Yngvason.
\newblock The physics and mathematics of the second law of thermodynamics.
\newblock \emph{Physics Reports}, 310\penalty0 (1):\penalty0 1--96, 1999.

\bibitem[Lovadina and Stenberg(2006)]{Lovadina2006}
C.~Lovadina and R.~Stenberg.
\newblock Energy norm a posteriori error estimates for mixed finite element
  methods.
\newblock \emph{Math. {C}omp.}, 75:\penalty0 1659 -- 1674, 2006.

\bibitem[Lubliner and Moran(1992)]{lubliner1992}
J.~Lubliner and B.~Moran.
\newblock Plasticity theory.
\newblock \emph{Journal of Applied Mechanics}, 59\penalty0 (1):\penalty0 245,
  1992.

\bibitem[Markvorsen(2006)]{markvorsen}
S.~Markvorsen.
\newblock \emph{The direct Flow parametric Proof of Gauss' Divergence Theorem
  revisited}.
\newblock Department of Mathematics, Technical University of Denmark, 2006.

\bibitem[Marsden et~al.(1984)Marsden, Hughes, and Carlson]{marsden1984}
J.~Marsden, T.~Hughes, and D.~Carlson.
\newblock Mathematical foundations of elasticity.
\newblock \emph{Journal of Applied Mechanics}, 51\penalty0 (4):\penalty0 946,
  1984.

\bibitem[Martinec(2019)]{martinec2019}
Z.~Martinec.
\newblock \emph{Principles of Continuum Mechanics: A Basic Course for
  Physicists}.
\newblock Springer, 2019.

\bibitem[Matolcsi and Ván(2006)]{MATOLCSI}
T.~Matolcsi and P.~Ván.
\newblock Can material time derivative be objective?
\newblock \emph{Physics Letters A}, 353\penalty0 (2):\penalty0 109--112, 2006.

\bibitem[McLean(2000)]{mclean}
W.~McLean.
\newblock \emph{Strongly elliptic systems and boundary integral equations}.
\newblock Cambridge university press, 2000.

\bibitem[Meyers and Serrin(1964)]{MeySer}
N.~Meyers and J.~Serrin.
\newblock H = w.
\newblock \emph{Proc. Nat. Acad. Sci. U.S.A.}, \penalty0 (51):\penalty0
  1055--1056, 1964.

\bibitem[Miehe et~al.(2014)Miehe, Welschinger, and Aldakheel]{MieheLog}
C.~Miehe, F.~Welschinger, and F.~Aldakheel.
\newblock Variational gradient plasticity at finite strains. part ii:
  Local–global updates and mixed finite elements for additive plasticity in
  the logarithmic strain space.
\newblock \emph{Computer Methods in Applied Mechanics and Engineering},
  268:\penalty0 704--734, 2014.

\bibitem[Morley(1989)]{Morley1989}
M.~E. Morley.
\newblock A family of mixed finite elements for linear elasticity.
\newblock \emph{Numerische Mathematik}, 55\penalty0 (6):\penalty0 633--666, Nov
  1989.

\bibitem[Munkres(1974)]{MunkresTop}
J.~Munkres.
\newblock \emph{Topology: A first course}.
\newblock Pearson College, 1 edition, 1974.

\bibitem[Nakahara(2003)]{NakaTop}
M.~Nakahara.
\newblock \emph{Geometry, Topology and Physics}.
\newblock CRC Press, 2 edition, 2003.

\bibitem[Necas(1962)]{Necas1962}
J.~Necas.
\newblock Sur une methode pour resoudre les equations aux derivees partielles
  du type elliptique, voisine de la variationnelle.
\newblock \emph{Annali della Scuola Normale Superiore di Pisa - Classe di
  Scienze}, 16\penalty0 (4):\penalty0 305--326, 1962.

\bibitem[Nedelec(1980)]{Nedelec1980}
J.~C. Nedelec.
\newblock Mixed finite elements in {$\mathbb{R}^{3}$}.
\newblock \emph{Numerische {M}athematik}, 35\penalty0 (3):\penalty0 315--341,
  Sep 1980.

\bibitem[Oden(1972)]{Oden1972}
J.~Oden.
\newblock \emph{Finite Element of Nonlinear Continua}.
\newblock McGraw-Hill, 1972.

\bibitem[Parisch(1986)]{Parisch}
H.~Parisch.
\newblock Efficient non-linear finite element shell formulation involving large
  strains.
\newblock \emph{Engng. Comp}, \penalty0 (3):\penalty0 121 -- 128, 1986.

\bibitem[Rudin(1987)]{Rudin}
W.~Rudin.
\newblock \emph{Real and complex analysis}.
\newblock McGraw-Hill Book Co., 1987.

\bibitem[Silhavy(2013)]{silhavy2013}
M.~Silhavy.
\newblock \emph{The mechanics and thermodynamics of continuous media}.
\newblock Springer Science \& Business Media, 2013.

\bibitem[Simo and Hughes(1998{\natexlab{a}})]{SimoPlastAlgo}
J.~Simo and T.~Hughes.
\newblock \emph{Computational Inelasticity}, pages 113--153.
\newblock Springer New York, NY, 1998{\natexlab{a}}.

\bibitem[Simo and Hughes(1998{\natexlab{b}})]{SimoPlastTheo}
J.~Simo and T.~Hughes.
\newblock \emph{Computational Inelasticity}, pages 71--112.
\newblock Springer New York, NY, 1998{\natexlab{b}}.

\bibitem[Stampacchia(1963)]{Stampa}
G.~Stampacchia.
\newblock Equations elliptiques du second ordre a coefficient discontinus
  [second-order elliptic equations with discontinuous coefficients].
\newblock \emph{Seminaire Jean Leray (College de France, Paris)}, \penalty0
  (3):\penalty0 1--77, 1963.

\bibitem[Stein(1970)]{Stein}
E.~Stein.
\newblock \emph{Singular Integrals and Differentiability Properties of
  Functions}, volume~30.
\newblock Princeton University Press, 1970.

\bibitem[Stenberg(1988)]{Stenberg1988}
R.~Stenberg.
\newblock A family of mixed finite elements for the elasticity problem.
\newblock \emph{Numerische {M}athematik}, 53\penalty0 (5):\penalty0 513--538,
  Aug 1988.

\bibitem[Stenberg(1997)]{stenberglow}
R.~Stenberg.
\newblock Two low-order mixed methods for the elasticity problem.
\newblock \emph{The mathematics of finite elements and applications}, pages
  271--280, 1997.

\bibitem[Stolze(1978)]{STOLZE}
C.~Stolze.
\newblock A history of the divergence theorem.
\newblock \emph{Historia Mathematica}, 5\penalty0 (4):\penalty0 437--442, 1978.

\bibitem[Suquet(1981)]{suquet1981equations}
P.~Suquet.
\newblock Sur les {\'e}quations de la plasticit{\'e}: existence et
  r{\'e}gularit{\'e} des solutions [elasticity equations: existence and
  regularity of solutions].
\newblock \emph{J. M{\'e}canique}, 20\penalty0 (1):\penalty0 3--39, 1981.

\bibitem[Sussman and Bathe(1987)]{Sussman1987}
T.~Sussman and K.~Bathe.
\newblock A finite element formulation for nonlinear incompressible elastic and
  inelastic analysis.
\newblock \emph{Computers \& {S}tructures}, 26\penalty0 (1):\penalty0 357 --
  409, 1987.

\bibitem[Tartar(2007)]{Tartar}
L.~Tartar.
\newblock \emph{An Introduction to Sobolev spaces and Interpolation spaces},
  volume~3.
\newblock Springer, Germany, 2007.

\bibitem[Turner et~al.(1956)Turner, Clough, Martin, and Topp]{Turner}
M.~Turner, R.~Clough, H.~C. Martin, and L.~T. Topp.
\newblock Stiffness and deflection analysis of complex structures.
\newblock \emph{J. Aeronaut. Sci}, 25:\penalty0 805--823, 1956.

\bibitem[Whitney(1957)]{Whitney}
H.~Whitney.
\newblock \emph{Geometric Integration Theory}.
\newblock Princeton University Press, 1957.

\bibitem[Wriggers et~al.(2011)Wriggers, Schroder, and Balzani]{Wriggers}
P.~Wriggers, J.~Schroder, and D.~Balzani.
\newblock A new mixed finite element based on different approximations of the
  minors of deformation tensors.
\newblock \emph{Computer methods in applied mechanics and engineering},
  200:\penalty0 3583--3600, 2011.

\bibitem[Zappino et~al.(2018)Zappino, Li, Pagani, Carrera, and {de
  Miguel}]{Zappino}
E.~Zappino, G.~Li, A.~Pagani, E.~Carrera, and A.~{de Miguel}.
\newblock Use of higher-order legendre polynomials for multilayered plate
  elements with node-dependent kinematics.
\newblock \emph{Composite Structures}, 202:\penalty0 222--232, 2018.

\bibitem[Zhang et~al.(2022{\natexlab{a}})Zhang, Constantinescu, and
  Smith]{PetscTSsolver}
H.~Zhang, E.~M. Constantinescu, and B.~F. Smith.
\newblock {PETSc TSAdjoint: A Discrete Adjoint ODE Solver for First-Order and
  Second-Order Sensitivity Analysis}.
\newblock \emph{SIAM Journal on Scientific Computing}, 44\penalty0
  (1):\penalty0 C1--C24, 2022{\natexlab{a}}.

\bibitem[Zhang et~al.(2022{\natexlab{b}})Zhang, Brown, Balay, Faibussowitsch,
  Knepley, Marin, Mills, Munson, Smith, and Zampini]{PetscParallel}
J.~Zhang, J.~Brown, S.~Balay, J.~Faibussowitsch, M.~Knepley, O.~Marin, R.~T.
  Mills, T.~Munson, B.~F. Smith, and S.~Zampini.
\newblock The {PetscSF} scalable communication layer.
\newblock \emph{IEEE Transactions on Parallel and Distributed Systems},
  33\penalty0 (4):\penalty0 842--853, 2022{\natexlab{b}}.

\bibitem[Zienkiewicz and Cheung(1965)]{zienkiewicz1965}
O.~Zienkiewicz and Y.~Cheung.
\newblock Finite elements in the solution of field problems.
\newblock \emph{The Engineer}, 220\penalty0 (5722):\penalty0 507--510, 1965.

\bibitem[Zienkiewicz and Cheung(1967)]{zienkiewicz1967finite}
O.~Zienkiewicz and Y.~Cheung.
\newblock \emph{The finite element method in structural mechanics}.
\newblock McGraw-Hill, London, 1967.

\bibitem[Zienkiewicz and Taylor(2000)]{zienkiewicz2000finite}
O.~Zienkiewicz and R.~L. Taylor.
\newblock \emph{The finite element method: solid mechanics}, volume~2.
\newblock Butterworth-heinemann, 2000.

\end{thebibliography}
