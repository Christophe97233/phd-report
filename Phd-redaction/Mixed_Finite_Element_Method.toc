\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {chapter}{Declaration of Authorship}{iii}{section*.1}
\contentsline {chapter}{Abstract}{vii}{section*.2}
\contentsline {chapter}{Acknowledgements}{ix}{section*.3}
\contentsline {chapter}{\numberline {1}Mixed Finite Formulation Theory}{1}{chapter.14}
\contentsline {section}{\numberline {1.1}Motivation for Mixed Formulation}{1}{section.15}
\contentsline {section}{\numberline {1.2}Results from functional analysis}{1}{section.16}
\contentsline {section}{\numberline {1.3}Small strain elasticity}{1}{section.19}
\contentsline {subsection}{\numberline {1.3.1}Definitions from functional analysis}{1}{subsection.20}
\contentsline {subsection}{\numberline {1.3.2}Classical form of the mixed problem}{2}{subsection.31}
\contentsline {subsection}{\numberline {1.3.3}Stability results}{3}{subsection.38}
\contentsline {section}{\numberline {1.4}Discretisation}{4}{section.54}
\contentsline {subsection}{\numberline {1.4.1}Galerkin scheme}{4}{subsection.55}
\contentsline {subsection}{\numberline {1.4.2}Spaces of polynomials}{5}{subsection.64}
\contentsline {subsection}{\numberline {1.4.3}Local and Global Interpolation Operators}{5}{subsection.69}
\contentsline {subsection}{\numberline {1.4.4}Global interpolation error and local estimates}{7}{subsection.86}
\contentsline {section}{\numberline {1.5}Application to Small Strains Elasticity}{8}{section.100}
\contentsline {subsection}{\numberline {1.5.1}Strong form}{8}{subsection.101}
\contentsline {subsection}{\numberline {1.5.2}Mixed Formulation}{9}{subsection.108}
\contentsline {section}{\numberline {1.6}Discretisation and link with deRham cohomology}{9}{section.109}
\contentsline {section}{\numberline {1.7}Results obtained with MoFEM}{9}{section.110}
