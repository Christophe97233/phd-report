#! /bin/bash
#----------------------------------------------------------

# This script use parametric study for viscosity parameter
# for eshelbian plasticity module

#----------------------------------------------------------

# Partition mesh file
for nn in `ls examples/torsion_beam/torsion_*.cub | sort -V`
#for nn in `ls examples/cook_cantilever/cook_34320.cub | sort -V`
do
    s_prev=${nn#examples/torsion_beam/}
    s_cut=${s_prev%.cub}
    meshsize=${s_cut#torsion_}
    if [ $meshsize != "48" ]
     then
    #NBPROC=2 && ../tools/mofem_part -my_file ./examples/cook_cantilever/bending_simple.cub -output_file bending_simple.h5m -my_nparts $NBPROC
    #NBPROC=16 && ../tools/mofem_part -my_file ./examples/cook_cantilever/$s_prev -output_file $s_cut.h5m -my_nparts $NBPROC
    NBPROC=8 && ../tools/mofem_part -my_file ./examples/torsion_beam/$s_prev -output_file $s_cut.h5m -my_nparts $NBPROC
    else
    NBPROC=1 && ../tools/mofem_part -my_file ./examples/torsion_beam/$s_prev -output_file $s_cut.h5m -my_nparts $NBPROC
    fi
done
#FILE="eshelbian_$s_cut"
#if [ ! -d "$FILE" ]
#then
#mkdir eshelbian_$s_cut
#else
 #   echo "existence of polluting vtk files erasing process in progress"
#
 #           for k in `ls eshelbian_$s_cut/out_*.vtk | sort -V`;
  #          do
   #         rm -f $k
   #         echo "File "$k" has been deleted";
    #        done;
#fi
for m in `ls examples/torsion_beam/torsion_*.cub | sort -V`
#for m in `ls examples/cook_cantilever/cook_34320.cub | sort -V`
do
    s_prev=${m#examples/torsion_beam/}
    s_cut=${s_prev%.cub}
    meshsize=${s_cut#torsion_}
#--------------------------------
# Erase potential polluting files 
#--------------------------------
    for n in `ls out_*.vtk | sort -V`
    do
        rm -f $n
        echo "File "$n" has been deleted"
    done;

    for o in `ls out_*.h5m | sort -V`
    do
        rm -f $o
        echo "File "$o" has been deleted"
    done
#----------------------------------
# Run analysis for parametric study
#----------------------------------
    for visc in 0
    do
        FILE="eshelbian_${s_cut}_visco_${visc}"
        if [ ! -d "$FILE" ]
        then
            mkdir eshelbian_${s_cut}_visco_${visc}
        else
            echo "existence of polluting vtk files erasing process in progress"
            for k in `ls eshelbian_${s_cut}_visco_${visc}/out_*.vtk | sort -V`
            do
                rm -f $k
                echo "File "$k" has been deleted"
            done
        fi
         if [ $meshsize != "48" ]
     then
# Run analysis and save results in folder
    /home/christophe/um_view/bin/mpirun -np 8 ./ep -my_file ${s_cut}.h5m -space_ghost_frame_on 0 -preconditioner_eps 1e-4 -space_order 2 -viscosity_alpha_u $visc 2>&1 | tee eshelbian_${s_cut}_visco_${visc}/log_mesh_${meshsize}_${visc}
        #/home/christophe/um_view/bin/mpirun -np 16 ./ep -my_file cook_34320.h5m -space_ghost_frame_on 0 -preconditioner_eps 1e-4 -space_order 2 -viscosity_alpha_u $visc 2>&1 | tee eshelbian_${s_cut}_visco_${visc}/log_mesh_${meshsize}_${visc}
        else
        /home/christophe/um_view/bin/mpirun -np 1 ./ep -my_file ${s_cut}.h5m -space_ghost_frame_on 0 -preconditioner_eps 1e-4 -space_order 2 -viscosity_alpha_u $visc 2>&1 | tee eshelbian_${s_cut}_visco_${visc}/log_mesh_${meshsize}_${visc}
        fi
# Convert files into vtk
        for l in `ls *.h5m | sort -V`;
        do
            mbconvert $l "$(basename "$l" .h5m).vtk";
            echo $l;
        done

# Save vtk and restart files in specific folder
        for p in `ls out_sol_elastic_*.vtk | sort -V`;
        do
            mv $p ./eshelbian_${s_cut}_visco_${visc}
            mv restart_* ./eshelbian_${s_cut}_visco_${visc}
            echo $p
        done

# Clean module if necessary
    CLEAN="out_sol_elastic_0.h5m"
        if [ -f "$CLEAN" ]
        then
            echo "Existence of vtk or h5m files. Start erasing"
            for q in `ls out_*.vtk | sort -V`
            do
                rm -f $q
                echo "File "$q" has been deleted"
            done

            for r in `ls out_*.h5m | sort -V`
            do
                rm -f $r
                echo "File "$r" has been deleted"
            done

        else
            echo "No vtk's it's fine :)"
        fi
        #sleep 60
        #rm -rf eshelbian_cook_*
    done
    rm $s_cut.vtk
    rm $s_cut.h5m
done