#! /bin/bash

for i in `ls *.h5m | sort -V`;
do
 mbconvert $i "$(basename "$i" .h5m).vtk";
echo $i;
done;
