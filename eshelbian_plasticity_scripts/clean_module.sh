#!/bin/bash
# This script cleans your module of out.vtk files if necessary



for i in `ls out_*.vtk | sort -V`;
do
 rm -f $i
echo "File "$i" has been deleted";
done;

for j in `ls out_*.h5m | sort -V`;
do
 rm -f $j
echo "File "$j" has been deleted";
done;

for k in `ls plast_out_*.vtk | sort -V`;
do
 rm -f $k
echo "File "$k" has been deleted";
done;

