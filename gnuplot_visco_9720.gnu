# This gnuplot script generates .ps graphs for displacement - force and crack area - energy relations comparing different runs (element orders ...)

set terminal postscript color enhanced "Verdana" 12


#Set number of steps for title
n_step = 15
#number of elements for title
n_elem = 9720
#Maximal load for analysis
tau_max = 300
dataf = "/Users/christophechalons/mofem_install/um/build/eshelbian_plasticity/data_cook_"
###########################################################
# SET PATHS AND LEGENDS
###########################################################

path_1 = sprintf("%s%d%s", dataf, n_elem ,"/")
path_2 = "/Users/christophechalons/mofem_install/um/build/eshelbian_plasticity/"
legend_0 = "viscosity 0"

#legend_1 = "viscosity 1"

#legend_2 = "viscosity 2"

#legend_3 = "viscosity 3"

#legend_4 = "viscosity 4"

#legend_5 = "viscosity 5"

legend_6 = "viscosity 6"

legend_7 = "viscosity 7"

legend_8 = "viscosity 8"

#legend_9 = "viscosity 9"

#legend_10 = "viscosity 10"



###########################################################
# LOAD FILES
###########################################################

data_file_0 = "stress_step_visco_0.txt"
#data_file_1 = "stress_step_visco_1.txt"
#data_file_2 = "stress_step_visco_2.txt"
#data_file_3 = "stress_step_visco_3.txt"
#data_file_4 = "stress_step_visco_4.txt"
#data_file_5 = "stress_step_visco_5.txt"
data_file_6 = "stress_step_visco_6.txt"
data_file_7 = "stress_step_visco_7.txt"
data_file_8 = "stress_step_visco_8.txt"
#data_file_9 = "stress_step_visco_9.txt"
#data_file_10 = "stress_step_visco_10.txt"
##
##
data_file_11 = "time_disp_visco_0.txt"
#data_file_12 = "time_disp_visco_1.txt"
#data_file_13 = "time_disp_visco_2.txt"
#data_file_14 = "time_disp_visco_3.txt"
#data_file_15 = "time_disp_visco_4.txt"
#data_file_16 = "time_disp_visco_5.txt"
data_file_17 = "time_disp_visco_6.txt"
data_file_18 = "time_disp_visco_7.txt"
data_file_19 = "time_disp_visco_8.txt"
#data_file_20 = "time_disp_visco_9.txt"
#data_file_21 = "time_disp_visco_10.txt"
##
##
data_file_22 = "stress_disp_visco_0.txt"
#data_file_23 = "stress_disp_visco_1.txt"
#data_file_24 = "stress_disp_visco_2.txt"
#data_file_25 = "stress_disp_visco_3.txt"
#data_file_26 = "stress_disp_visco_4.txt"
#data_file_27 = "stress_disp_visco_5.txt"
data_file_28 = "stress_disp_visco_6.txt"
data_file_29 = "stress_disp_visco_7.txt"
data_file_30 = "stress_disp_visco_8.txt"
#data_file_31 = "stress_disp_visco_9.txt"
#data_file_32 = "stress_disp_visco_10.txt"

###########################################################
# SET MAXIMUM EXTERNAL LOAD AND THRESHOLD FOR PLOTS
###########################################################

tau = 0.076
# Value at 5%
Threshold_1 = tau / 20
# Last value of the data file
Threshold_2 = system("tail -n 1 ./data_cook_48/stress_step_visco_1.txt | cut -d ' ' -f 1")+0
# Value at 25%
Threshold_3 = tau / 4
# Value at 50%
Threshold_4 = tau / 2
#Flag = 1

############################################################
# PLOT 1 : STRESS = F(TAU)
############################################################

set output path_2."stress_versus_time_visco.ps"
set title sprintf("Y-axis : Stress {/Symbol s}_{xx} (kPa), X-axis : External load {/Symbol t} (kPa) \n at point for maximum tensile stress, \n  number of elements = %d, number of steps = %d, {/Symbol t}_{max} = %d kPa ", n_elem, n_step, tau_max) font ",13" enhanced
set xlabel "External load {/Symbol t} (kPa)"
set ylabel "{/Symbol s}_{xx} (kPa)" enhanced
set format x "%5.1e"
set format y "%5.1e"
 set grid ytics xtics mytics
#set xtics 0,2,50
#set ytics 0,.05,1
unset key
set key right bottom #box opaque
set style textbox opaque
#set yrange [0:0.3]
#set xrange [0:0.02]
plot path_1.data_file_0 u 1:2 title legend_0 w lp  pt 2 ps 1.2 lw 4 dt 0 lc rgb "brown"  pointinterval 1, \
 path_1.data_file_6 u 1:2 title legend_6 w lp  pt 1 ps 1.2 lw 4 dt 0 lc rgb "cyan"  pointinterval 1, \
 path_1.data_file_7 u 1:2 title legend_7 w lp  pt 8 ps 1.2 lw 6 dt 0 lc rgb "magenta" pointinterval 1, \
 path_1.data_file_8 u 1:2 title legend_8 w lp  pt 9 ps 1.2 lw 8 dt 0 lc rgb "violet"  pointinterval 1

 #path_1.data_file_1 u 1:2 title legend_1 w lp  pt 3 ps 1.2 lw 6 dt 0 lc rgb "blue"  pointinterval 1, \
 #path_1.data_file_2 u 1:2 title legend_2 w lp  pt 4 ps 1.2 lw 8 dt 0 lc rgb "purple"  pointinterval 1, \
 #path_1.data_file_3 u 1:2 title legend_3 w lp  pt 5 ps 1.2 lw 8 dt 0 lc rgb "green"  pointinterval 1, \
 #path_1.data_file_4 u 1:2 title legend_4 w lp  pt 6 ps 1.2 lw 8 dt 0 lc rgb "red"  pointinterval 1, \
 #path_1.data_file_5 u 1:2 title legend_5 w lp  pt 7 ps 1.2 lw 8 dt 0 lc rgb "orange"  pointinterval 1, \
 #path_1.data_file_9 u 1:2 title legend_9 w lp  pt 10 ps 1.2 lw 8 dt 0 lc rgb "black"  pointinterval 1, \
 #path_1.data_file_10 u 1:2 title legend_10 w lp  pt 11 ps 1.2 lw 8 dt 0 lc rgb "grey"  pointinterval 1
#path_1.data_file_1  u 1:2:(($1==Threshold_1) || ($1==Threshold_2) || ($1==Threshold_3) || ($1==Threshold_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels center boxed notitle, \
#path_1.data_file_2  u 1:2:(($1==Threshold_1) || ($1==Threshold_2) || ($1==Threshold_3) || ($1==Threshold_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels point  pt 6 offset char -12,0  notitle 
  #path_1.data_file_3 u 1:2 title legend_3 w lp pt 6 ps 1.2 lw 4 dt 3 lc rgb "blue"  pointinterval 1, \
    #path_1.data_file_4 u 1:2 title legend_4 w lp pt 8 ps 1.2 lw 4 dt 6 lc rgb "red"  pointinterval 1, \
    #path_1.data_file_5 u 1:2 title legend_5 w lp pt 10 ps 1.2 lw 4 dt 8 lc rgb "green"  pointinterval 1, \
        #path_1.data_file_6 u 1:2 title legend_6 w lp pt 8 ps 1.2 lw 4 dt 6 lc rgb "orange"  pointinterval 1
        #path_1.data_file_2 u 1:2 title legend_2 w lp pt 2 ps 1.2 lw 4 dt 0 lc rgb "brown"  pointinterval 1, \
#plot data_file u 1:2 title legend_1 w lp pt 2 ps 1.2 lw 4 dt 0 lc rgb "brown"  pointinterval 1
                    #path_2.data_file u 1:2 title legend_2 w lp pt 4 ps 1.2 lw 4 dt 2 lc rgb "violet"  pointinterval 1, \
                    #path_3.data_file u 1:2 title legend_3 w lp pt 6 ps 1.2 lw 4 dt 3 lc rgb "blue"  pointinterval 1, 
                    # path_4.data_file u 2:3 title legend_4 w lp pt 8 ps 1.2 lw 4 dt 6 lc rgb "red"  pointinterval 1
#labels point  pt 7 offset char 1,1 
#unset yrange
#unset xrange
############################################################
# PLOT 2 : DISP = F(TAU)
############################################################

set output path_2."Displacement_vs_time_visco.ps"
 set title sprintf("Y-axis : Displacement w_x (mm), X-axis : External load {/Symbol t} (kPa) \n at point C (Wriggers), \n number of elements = %d, number of steps = %d, {/Symbol t}_{max} = %d kPa ", n_elem, n_step, tau_max) font ",13" enhanced
 set xlabel "Displacement w_x (m)"
 set ylabel "External load {/Symbol t} (kPa)" enhanced
 set grid ytics xtics mytics
 set format x "%5.1e"
set format y "%5.1e"
unset key
 set key right bottom # box
#set yrange [0:0.02]
#set xrange [0:0.01]
Threshold_5 = system("tail -n 1 ./data_cook_48/time_disp_visco_1.txt | cut -d ' ' -f 2")+0
#Flag = 1
plot path_1.data_file_11 u 1:2 title legend_0 w lp  pt 2 ps 1.2 lw 4 dt 0 lc rgb "brown"  pointinterval 1, \
 path_1.data_file_17 u 1:2 title legend_6 w lp  pt 8 ps 1.2 lw 6 dt 0 lc rgb "cyan" pointinterval 1, \
 path_1.data_file_18 u 1:2 title legend_7 w lp  pt 9 ps 1.2 lw 8 dt 0 lc rgb "magenta"  pointinterval 1, \
 path_1.data_file_19 u 1:2 title legend_8 w lp  pt 10 ps 1.2 lw 8 dt 0 lc rgb "violet"  pointinterval 1
#path_1.data_file_20 u 1:2 title legend_9 w lp  pt 11 ps 1.2 lw 8 dt 0 lc rgb "black"  pointinterval 1, \
#path_1.data_file_21 u 1:2 title legend_10 w lp  pt 3 ps 1.2 lw 6 dt 0 lc rgb "grey"  pointinterval 1
#path_1.data_file_12 u 1:2 title legend_1 w lp  pt 4 ps 1.2 lw 8 dt 0 lc rgb "blue"  pointinterval 1, \
#path_1.data_file_13 u 1:2 title legend_2 w lp  pt 5 ps 1.2 lw 8 dt 0 lc rgb "purple"  pointinterval 1, \
#path_1.data_file_14 u 1:2 title legend_3 w lp  pt 6 ps 1.2 lw 8 dt 0 lc rgb "green"  pointinterval 1, \
#path_1.data_file_15 u 1:2 title legend_4 w lp  pt 7 ps 1.2 lw 8 dt 0 lc rgb "red"  pointinterval 1, \
#path_1.data_file_16 u 1:2 title legend_5 w lp  pt 1 ps 1.2 lw 4 dt 0 lc rgb "orange"  pointinterval 1, \
#path_1.data_file_20 u 1:2 title legend_9 w lp  pt 11 ps 1.2 lw 8 dt 0 lc rgb "black"  pointinterval 1, \
#path_1.data_file_21 u 1:2 title legend_10 w lp  pt 3 ps 1.2 lw 6 dt 0 lc rgb "grey"  pointinterval 1
#path_1.data_file_4  u 1:2:(($2==Threshold_1) || ($2==Threshold_5) || ($2==Threshold_3) || ($2==Threshold_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels center boxed notitle, \
#                     path_2.data_file every ::1 u 4:($5*1) title legend_2 w lp pt 4 ps 1.2 lw 4 dt 2 lc rgb "violet"  pointinterval 1, \
#                     path_4.data_file every ::1 u 4:($5*1) title legend_4 w lp pt 8 ps 1.2 lw 4 dt 6 lc rgb "red"  pointinterval 1
#                     path_3.data_file every ::1 u 4:($5*1) title legend_3 w lp pt 6 ps 1.2 lw 4 dt 3 lc rgb "blue"  pointinterval 1, \


############################################################
# PLOT 3 : DISP = F(SIGMA_XX)
############################################################


set output path_2."Displacement_vs_stress_visco.ps"
 set title sprintf("Y-axis Displacement w_x (mm), X-axis : stress {/Symbol s}_{xx} (kPa) \n at point for maximum tensile stress, \n number of elements = %d, number of steps = %d, {/Symbol t}_{max} = %d kPa ", n_elem, n_step, tau_max) font ",13" enhanced
 set xlabel "Displacement w_x (m)"
 set ylabel "{/Symbol s}_{xx} (kPa)" enhanced
  set grid ytics xtics mytics
  set format x "%5.1e"
set format y "%5.1e"
unset key
 set key left top # box
 #set yrange [0:0.3]
#set xrange [0:0.01]

 round(x) = x - floor(x) < 0.5 ? floor(x) : ceil(x)

# Find the line for 5% of the load
line_1 = round(n_step/20) + 1
command_12 = sprintf("cat ./data_cook_48/stress_disp_visco_1.txt | awk 'NR==%d' | tee content_1.dat | cut -d ' ' -f 1", line_1)
disp_value_1 = system(command_12)+0

# Find the line for 25% of the load
line_2 = round(n_step/4) + 1 
command_21 = sprintf("cat ./data_cook_48/stress_disp_visco_1.txt | awk 'NR==%d' | tee content_2.dat | cut -d ' ' -f 1", line_2)
#disp_value_2 = system(command_21)+0 #UNCOMMENT IFF VALUE EXISTS IN DATAFILE
# Find the line for 50% of the load
line_3 = round(n_step/2) + 1
command_32 = sprintf("cat ./data_cook_48/stress_disp_visco_1.txt | awk 'NR==%d' | tee content_3.dat | cut -d ' ' -f 1", line_3)
#disp_value_3 = system(command_32)+0 #UNCOMMENT IFF VALUE EXISTS IN DATAFILE
# Find the line for the last converged step
disp_value_4 = system("tail -n 1 ./data_cook_48/stress_disp_visco_1.txt | cut -d ' ' -f 1")+0
 plot path_1.data_file_22  u 1:2 title legend_0 w lp pt 2 ps 1.2 lw 4 dt 0 lc rgb "brown"   pointinterval 1, \
  path_1.data_file_28 u 1:2 title legend_6 w lp  pt 1 ps 1.2 lw 4 dt 0 lc rgb "cyan"  pointinterval 1, \
  path_1.data_file_29 u 1:2 title legend_7 w lp  pt 8 ps 1.2 lw 6 dt 0 lc rgb "magenta"pointinterval 1, \
  path_1.data_file_30 u 1:2 title legend_8 w lp  pt 9 ps 1.2 lw 8 dt 0 lc rgb "violet"  pointinterval 1
    #path_1.data_file_23  u 1:2 title legend_1 w lp pt 3 ps 1.2 lw 6 dt 0 lc rgb "blue"  pointinterval 1, \
  #path_1.data_file_24  u 1:2 title legend_2 w lp pt 4 ps 1.2 lw 8 dt 0 lc rgb "purple"  pointinterval 1, \
  #path_1.data_file_25  u 1:2 title legend_3 w lp pt 3 ps 1.2 lw 6 dt 0 lc rgb "green"  pointinterval 1, \
  #path_1.data_file_26  u 1:2 title legend_4 w lp pt 3 ps 1.2 lw 6 dt 0 lc rgb "red"  pointinterval 1, \
  #path_1.data_file_27  u 1:2 title legend_5 w lp pt 3 ps 1.2 lw 6 dt 0 lc rgb "orange"  pointinterval 1, \
  #path_1.data_file_31 u 1:2 title legend_9 w lp  pt 10 ps 1.2 lw 8 dt 0 lc rgb "black"  pointinterval 1, \
  #path_1.data_file_32 u 1:2 title legend_10 w lp  pt 11 ps 1.2 lw 8 dt 0 lc rgb "grey"  pointinterval 1
  #path_1.data_file_7  u 1:2:(($1==disp_value_1) || ($1==disp_value_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "") w labels center boxed notitle, \
 #path_1.data_file_3  u 1:2:(($1==disp_value_1) || ($1==disp_value_2) || ($1==disp_value_3) || ($1==disp_value_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels point  pt 6 offset char -12,0 notitle 
#                     path_2.data_file every ::1 u 4:($5*1) title legend_2 w lp pt 4 ps 1.2 lw 4 dt 2 lc rgb "violet"  pointinterval 1, \
#                     path_3.data_file every ::1 u 4:($5*1) title legend_3 w lp pt 6 ps 1.2 lw 4 dt 3 lc rgb "blue"  pointinterval 1, \
#                     path_4.data_file every ::1 u 4:($5*1) title legend_4 w lp pt 8 ps 1.2 lw 4 dt 6 lc rgb "red"  pointinterval 1

############################################################
# MULTIPLOT (CANNOT BE EXPLOITED YET)
############################################################

set output "Multiplot.ps"
set multiplot layout 2, 2 title sprintf("Full analysis \n at point for maximum tensile stress, \n mesh with %d elements, number of steps = %d, {/Symbol t}_{max} = %d kPa ", n_elem, n_step, tau_max) font ",13"
set title "Y-axis : Stress {/Symbol s}_{xx} (MPa), X-axis : External load {/Symbol t} (MPa)"
set xlabel "Displacement w_x (m)"
 set ylabel "{/Symbol s}_{xx} (MPa)" enhanced
  set grid ytics xtics mytics
  set format x "%5.1e"
set format y "%5.1e"
unset key
 set key right # box
plot path_1.data_file_2 u 1:2 title legend_2 w lp  pt 2 ps 1.2 lw 4 dt 0 lc rgb "brown"  pointinterval 1, \
path_1.data_file_2  u 1:2:(($1==Threshold_1) || ($1==Threshold_2) || ($1==Threshold_3) || ($1==Threshold_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels point  pt 6 offset char -12,0  notitle 
#
#
set title "Y-axis : Displacement w_x (m), X-axis : External load {/Symbol t} (MPa)" 
 set xlabel "Displacement w_x (m)"
 set ylabel "External load {/Symbol t} (MPa)" enhanced
 set grid ytics xtics mytics
 set format x "%5.1e"
set format y "%5.1e"
unset key
 set key right # box
 plot path_1.data_file_1  u 1:2 title legend_2 w lp pt 2 ps 1.2 lw 4 dt 0 lc rgb "blue"  pointinterval 1, \
 path_1.data_file_1  u 1:2:(($2==Threshold_1) || ($2==Threshold_5) || ($2==Threshold_3) || ($2==Threshold_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels point  pt 6 offset char -12,0 notitle 
 #
 #
 set title "Y-axis Displacement w_x (m), X-axis : stress {/Symbol s}_{xx} (MPa)"
  set xlabel "Displacement w_x (m)"
 set ylabel "{/Symbol s}_{xx} (MPa)" enhanced
  set grid ytics xtics mytics
  set format x "%5.1e"
set format y "%5.1e"
unset key
 set key right # box
 plot path_1.data_file_3  u 1:2 title legend_2 w lp pt 2 ps 1.2 lw 4 dt 0 lc rgb "green"   pointinterval 1, \
 path_1.data_file_3  u 1:2:(($1==disp_value_1) || ($1==disp_value_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels point  pt 6 offset char -12,0 notitle
 #path_1.data_file_3  u 1:2:(($1==disp_value_1) || ($1==disp_value_2) || ($1==disp_value_3) || ($1==disp_value_4) ? (sprintf("(%5.3e, %5.3e)", $1, $2)) : "")  w labels point  pt 6 offset char -12,0 notitle 
unset multiplot