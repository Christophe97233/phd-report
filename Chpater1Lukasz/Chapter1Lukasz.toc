\contentsline {section}{\numberline {1}Elements of Continuum mechanics and Mathematical theory for nonlinear and non-dissipative elliptic problems}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Thermodynamics Principles}{2}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}The first law of thermodynamics}{2}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}The second law of thermodynamics}{4}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Thermodynamics with internal variables}{5}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Theory of hyperelasticity}{6}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Overview}{6}{subsubsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.2}Logarithmic strains}{10}{subsubsection.1.3.2}
\contentsline {subsection}{\numberline {1.4}Different types of functionals}{10}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}Gibbs}{10}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}Castigliano}{10}{subsubsection.1.4.2}
\contentsline {subsubsection}{\numberline {1.4.3}Hashin-Strikman}{10}{subsubsection.1.4.3}
\contentsline {subsubsection}{\numberline {1.4.4}Rayleigh's principle (dynamics)}{10}{subsubsection.1.4.4}
\contentsline {subsubsection}{\numberline {1.4.5}Classical Lagrangian}{10}{subsubsection.1.4.5}
\contentsline {subsubsection}{\numberline {1.4.6}Hellinger-Reissner}{10}{subsubsection.1.4.6}
\contentsline {subsubsection}{\numberline {1.4.7}Modified Hellinger-Reissner}{10}{subsubsection.1.4.7}
\contentsline {subsubsection}{\numberline {1.4.8}Hu-Washizu}{11}{subsubsection.1.4.8}
\contentsline {subsection}{\numberline {1.5}Spaces}{11}{subsection.1.5}
\contentsline {subsubsection}{\numberline {1.5.1}$L^2$}{11}{subsubsection.1.5.1}
\contentsline {subsubsection}{\numberline {1.5.2}Sobolev space $H^1$}{11}{subsubsection.1.5.2}
\contentsline {subsubsection}{\numberline {1.5.3}$H_\mathrm {div}$}{11}{subsubsection.1.5.3}
\contentsline {subsection}{\numberline {1.6}Approximation of infinite dimensional spaces on Finite Element}{11}{subsection.1.6}
\contentsline {section}{\numberline {2}Results from functional analysis}{11}{section.2}
\contentsline {section}{\numberline {3}The Mixed formulation for the Poisson problem}{12}{section.3}
\contentsline {section}{\numberline {4}Small strain elasticity}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Definitions from functional analysis}{12}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Classical form of the mixed problem}{13}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Stability results}{14}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Possible applications}{15}{subsection.4.4}
\contentsline {section}{\numberline {5}My element (Mixed finite element formulation)}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}From the modified Hellinger-Reissner principle to the discretized problem}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Discretization}{15}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Description of the element}{15}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Algebra}{15}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Representation of rotations}{15}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Schur Complement}{15}{subsubsection.5.4.2}
