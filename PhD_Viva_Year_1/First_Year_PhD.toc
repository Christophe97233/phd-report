\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Small Strain Plasticity Theory}{5}
\contentsline {section}{\numberline {1.1}A physical model for small strain plasticity}{5}
\contentsline {section}{\numberline {1.2}Deriving the plastic multiplier}{7}
\contentsline {section}{\numberline {1.3}Some plastic models}{8}
\contentsline {subsection}{\numberline {1.3.1}Von Mises Plastic model}{8}
\contentsline {subsection}{\numberline {1.3.2}Tresca Plastic model}{8}
\contentsline {subsection}{\numberline {1.3.3}Mohr Coulomb Plastic model}{9}
\contentsline {subsection}{\numberline {1.3.4}Drucker-Prager Plastic model}{9}
\contentsline {section}{\numberline {1.4}Flow rules}{9}
\contentsline {subsection}{\numberline {1.4.1}Associative Von Mises}{9}
\contentsline {subsection}{\numberline {1.4.2}Associative and non-associative Tresca}{10}
\contentsline {subsection}{\numberline {1.4.3}Associative and non-associative Mohr-Coulomb}{11}
\contentsline {subsection}{\numberline {1.4.4}Associative and non associative Drucker-Prager}{12}
\contentsline {section}{\numberline {1.5}Principle of maximum plastic Dissipation}{13}
\contentsline {section}{\numberline {1.6}Hardening}{13}
\contentsline {subsection}{\numberline {1.6.1}Perfect Plasticity}{13}
\contentsline {subsection}{\numberline {1.6.2}Isotropic Hardening}{14}
\contentsline {chapter}{\numberline {2}Small Strain Plasticity : Numerical Implementation }{16}
\contentsline {section}{\numberline {2.1}Elasto-Plastic beam in 1D using Matlab}{16}
\contentsline {section}{\numberline {2.2}A 3D elastoplastic beam in MoFEM}{19}
\contentsline {chapter}{\numberline {3}Configurational Mechanics}{22}
\contentsline {section}{\numberline {3.1}Elements of field theory}{22}
\contentsline {subsection}{\numberline {3.1.1}Euler-Lagrange expressions}{22}
\contentsline {subsection}{\numberline {3.1.2}Conservation Laws}{23}
\contentsline {subsection}{\numberline {3.1.3}Application to the case of hyperelasticity}{23}
\contentsline {subsection}{\numberline {3.1.4}Example: continuously layered material}{24}
\contentsline {chapter}{\numberline {4}Linear Elastic Fracture Mechanics : A brief overview}{25}
\contentsline {chapter}{\numberline {5}Numerical implementation using MoFEM}{28}
\contentsline {section}{\numberline {5.1}Overview of the organisation of the code for the problem of the perfectly plastic bar}{29}
