\begin{thebibliography}{10}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Abhyankar et~al.(2018)Abhyankar, Brown, Constantinescu, Ghosh, Smith,
  and Zhang]{abhyankar2018petsc}
S.~Abhyankar, J.~Brown, E.~M. Constantinescu, D.~Ghosh, B.~F. Smith, and
  H.~Zhang.
\newblock Petsc/ts: A modern scalable ode/dae solver library.
\newblock \emph{arXiv preprint arXiv:1806.01437}, 2018.

\bibitem[Balay et~al.(1997)Balay, Gropp, McInnes, and Smith]{petsc-efficient}
S.~Balay, W.~D. Gropp, L.~C. McInnes, and B.~F. Smith.
\newblock Efficient management of parallelism in object oriented numerical
  software libraries.
\newblock In E.~Arge, A.~M. Bruaset, and H.~P. Langtangen, editors,
  \emph{Modern Software Tools in Scientific Computing}, pages 163--202.
  Birkh{\"{a}}user Press, 1997.

\bibitem[Balay et~al.(2019{\natexlab{a}})Balay, Abhyankar, Adams, Brown, Brune,
  Buschelman, Dalcin, Dener, Eijkhout, Gropp, Karpeyev, Kaushik, Knepley, May,
  McInnes, Mills, Munson, Rupp, Sanan, Smith, Zampini, Zhang, and
  Zhang]{petsc-web-page}
S.~Balay, S.~Abhyankar, M.~F. Adams, J.~Brown, P.~Brune, K.~Buschelman,
  L.~Dalcin, A.~Dener, V.~Eijkhout, W.~D. Gropp, D.~Karpeyev, D.~Kaushik, M.~G.
  Knepley, D.~A. May, L.~C. McInnes, R.~T. Mills, T.~Munson, K.~Rupp, P.~Sanan,
  B.~F. Smith, S.~Zampini, H.~Zhang, and H.~Zhang.
\newblock {PETS}c {W}eb page.
\newblock \url{http://www.mcs.anl.gov/petsc}, 2019{\natexlab{a}}.
\newblock URL \url{http://www.mcs.anl.gov/petsc}.

\bibitem[Balay et~al.(2019{\natexlab{b}})Balay, Abhyankar, Adams, Brown, Brune,
  Buschelman, Dalcin, Dener, Eijkhout, Gropp, Karpeyev, Kaushik, Knepley, May,
  McInnes, Mills, Munson, Rupp, Sanan, Smith, Zampini, Zhang, and
  Zhang]{petsc-user-ref}
S.~Balay, S.~Abhyankar, M.~F. Adams, J.~Brown, P.~Brune, K.~Buschelman,
  L.~Dalcin, A.~Dener, V.~Eijkhout, W.~D. Gropp, D.~Karpeyev, D.~Kaushik, M.~G.
  Knepley, D.~A. May, L.~C. McInnes, R.~T. Mills, T.~Munson, K.~Rupp, P.~Sanan,
  B.~F. Smith, S.~Zampini, H.~Zhang, and H.~Zhang.
\newblock {PETS}c users manual.
\newblock Technical Report ANL-95/11 - Revision 3.11, Argonne National
  Laboratory, 2019{\natexlab{b}}.
\newblock URL \url{http://www.mcs.anl.gov/petsc}.

\bibitem[E.A~de Souza~Neto(2008{\natexlab{a}})]{DeSouzaMathematical}
D.~O. E.A~de Souza~Neto, D.~Peric.
\newblock \emph{The mathematical Theory of Plasticity}, chapter~6, pages
  137--190.
\newblock Wiley-Blackwell, 2008{\natexlab{a}}.

\bibitem[E.A~de Souza~Neto(2008{\natexlab{b}})]{DeSouzaNumerics}
D.~O. E.A~de Souza~Neto, D.~Peric.
\newblock \emph{Finite Elements in Small Strain Plasticity Problems},
  chapter~7, pages 191--263.
\newblock Wiley-Blackwell, 2008{\natexlab{b}}.

\bibitem[Han and Reddy(2012)]{Reddy}
W.~Han and B.~Reddy.
\newblock \emph{The Primal Variational Problem of Elastoplasticity}, pages
  187--201.
\newblock Springer, 2012.

\bibitem[Kaczmarczyk et~al.(2017)Kaczmarczyk, Z., K., Meng, Zhou, and
  C.]{mofem}
L.~Kaczmarczyk, U.~Z., L.~K., X.~Meng, X.-Y. Zhou, and P.~C.
\newblock Mofem-v0.5.42, Mar. 2017.
\newblock http://mofem.eng.gla.ac.uk/mofem/html/.

\bibitem[Simo and Hughes(1998{\natexlab{a}})]{Simo1998Numerics1}
J.~Simo and T.~Hughes.
\newblock \emph{Integration Algorithms for Plasticity and Viscoplasticity},
  pages 113--153.
\newblock Springer New York, New York, NY, 1998{\natexlab{a}}.

\bibitem[Simo and Hughes(1998{\natexlab{b}})]{Simo1998Theory}
J.~Simo and T.~Hughes.
\newblock \emph{Classical Rate-Independent Plasticity and Viscoplasticity},
  pages 71--112.
\newblock Springer New York, New York, NY, 1998{\natexlab{b}}.

\end{thebibliography}
