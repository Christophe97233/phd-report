\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {section}{\numberline {1}Elements of classical (small strains) plasticity}{3}
\contentsline {subsection}{\numberline {1.1}Thermodynamics Principles}{3}
\contentsline {subsubsection}{\numberline {1.1.1}The first law of thermodynamics}{3}
\contentsline {subsubsection}{\numberline {1.1.2}The second law of thermodynamics}{4}
\contentsline {subsection}{\numberline {1.2}Application to small strains plasticity}{5}
\contentsline {subsubsection}{\numberline {1.2.1}Thermodynamics with internal variables}{5}
\contentsline {subsubsection}{\numberline {1.2.2}Plasticity in small strains}{7}
\contentsline {subsubsection}{\numberline {1.2.3}The Von-Mises model with isotropic linear hardening}{9}
\contentsline {subsection}{\numberline {1.3}Variational formulation of elasto-plastic problem}{9}
\contentsline {subsubsection}{\numberline {1.3.1}Functional spaces}{9}
\contentsline {subsubsection}{\numberline {1.3.2}Linear and bilinear forms}{10}
\contentsline {subsubsection}{\numberline {1.3.3}Discussion on the primal variational problem}{10}
\contentsline {subsection}{\numberline {1.4}The return-mapping algorithm in small strains}{11}
\contentsline {section}{\numberline {2}Eshelbian plasticity}{13}
\contentsline {subsection}{\numberline {2.1}Kinematics}{13}
\contentsline {subsection}{\numberline {2.2}Consistency equation}{14}
\contentsline {subsection}{\numberline {2.3}Physical equation}{14}
\contentsline {subsection}{\numberline {2.4}Angular momentum equation}{14}
\contentsline {subsection}{\numberline {2.5}Linear momentum equation}{14}
\contentsline {section}{\numberline {3}Numerical implementation for some problems}{15}
\contentsline {subsection}{\numberline {3.1}1D problem : beam in traction}{15}
\contentsline {subsection}{\numberline {3.2}3D problem : Plate with circular hole in traction}{17}
\contentsline {section}{\numberline {4}Future Work}{21}
\contentsline {section}{\numberline {5}Conclusion}{22}
\contentsline {section}{\numberline {6}Gantt chart}{23}
\contentsline {section}{\numberline {A}Convex setting }{25}
\contentsline {subsection}{\numberline {A.1}Basic definitions for convex analysis}{25}
\contentsline {subsection}{\numberline {A.2}Vector space additional definitions}{25}
\contentsline {section}{\numberline {B}Functional analysis}{27}
\contentsline {subsection}{\numberline {B.1}Definitions and lemmas}{27}
\contentsline {subsection}{\numberline {B.2}Functional spaces : Predefinition}{28}
\contentsline {subsection}{\numberline {B.3}Hilbert spaces : Definition and properties}{31}
\contentsline {subsection}{\numberline {B.4}Function Spaces}{32}
\contentsline {subsection}{\numberline {B.5}Sobolev spaces}{34}
